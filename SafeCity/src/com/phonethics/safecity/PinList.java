package com.phonethics.safecity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.database.SQLException;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

public class PinList extends Activity {

	Asynch as;
	List<ParseObject> objects_test, objects_test1;
	String str, objid_title;
	String objid ;
	Activity con;

	PinListAdapter pinAdapter;
	ArrayList<String> address, categoryId, title, tempDb, tableUserId;

	ArrayList<Integer> matched;

	ArrayList<String> objectIdDb, dbuserId;

	ListView pinList;
	//ProgressBar prog;

	DbUtil dbutil;
	String curr_userId;

	ArrayList<String> objctIdFromCatTable, fetchTitles, tempTitle;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_pin_list);

		con = this;

		ParseUser currentUser = ParseUser.getCurrentUser();
		if (currentUser != null) {
			// do stuff with the user

			curr_userId = currentUser.getObjectId();
			//Toast.makeText(con, "" + curr_userId, 0).show();

		} else {
			// show the signup or login screen
		}

		dbutil = new DbUtil(con);
		//prog=(ProgressBar)findViewById(R.id.prog);

		address = new ArrayList<String>();
		categoryId = new ArrayList<String>();
		title = new ArrayList<String>();
		tempDb = new ArrayList<String>();
		tableUserId = new ArrayList<String>();
		matched = new ArrayList<Integer>();
		objectIdDb = new ArrayList<String>();
		objctIdFromCatTable = new ArrayList<String>();

		fetchTitles = new ArrayList<String>();
		dbuserId = new ArrayList<String>();

		pinList = (ListView) findViewById(R.id.pinList);

		categoryId = dbutil.catIdFunc();
		title = dbutil.catTitleFunc();

		tempTitle = new ArrayList<String>();

		//Log.d("ID","CATID------------" +  categoryId.size());

		/*as  = new Asynch();
		as.execute();*/

		try {

			objectIdDb = dbutil.objectIdFunc();
			objctIdFromCatTable = dbutil.catObjIDFunc();

			fetchTitles = dbutil.catIdFunc();

			dbuserId = dbutil.userIdFunc();

			for(int m=0;m<dbuserId.size();m++){

				Log.d("FECTHECATID","FECTHECATID  " + dbuserId.get(m));

				if(dbuserId.get(m).equals(curr_userId)){

					Log.d("MATCHEDID","MATCHEDID  " + m);
					matched.add(m);
				}
				else{
					matched.add(-1);
				}
			}

			//Log.d("CountValue", "CountValue === +++ fetchTitle " + objctIdFromCatTable.size());

			//Log.d("DBUSER","DBUSER" + dbuserId.size());

			for(int i=0;i<fetchTitles.size();i++){

				/*Log.d("FECTHECATID","FECTHECATID  " + dbutil.userIdFuncByCatId(fetchTitles.get(i)));
				Log.d("FECTHEDUID","FECTHEDUID  " + fetchTitles.get(i));*/
				/*	String chkforMatchedId = dbutil.userIdFuncByCatId(fetchTitles.get(i));
				Log.d("FECTHEDUID","FECTHEDUID" + chkforMatchedId);
				 */
				/*if(dbutil.userIdFuncByCatId(fetchTitles.get(i)).equalsIgnoreCase(curr_userId)){

					Log.d("PLEASECHK","PLEASECHK" + i);
					matched.add(i);
				}
				else{

					matched.add(-1);
					Log.d("NODATA","NODATA");
				}
				 */
				//Log.d("CountValue", "CountValue === +++ fetchTitle " + fetchTitles.get(i));
				for(int k=0;k<objctIdFromCatTable.size();k++){


					if(fetchTitles.get(i).equals(objctIdFromCatTable.get(k))){
						//Toast.makeText(con, ""+title.get(i)+"Size : "+title.size(), Toast.LENGTH_SHORT).show();
						tempTitle.add(title.get(k));



						//Log.d("CountValue", "GETTINTITLE== " + title.get(k));
					}
				}

			}

			// code to detect to place arrow or not ---------

			/*	try {

				//Toast.makeText(con, dbuserId.get(0), 0).show();
				Log.d("curruserid", "curruserid" + curr_userId);
				for(int i=0;i<dbuserId.size();i++){

					if(dbuserId.get(i).equalsIgnoreCase(curr_userId)){

						Log.d("PLEASECHK","PLEASECHK" + dbuserId.get(i));

						matched.add(i);
					}
					else{

						matched.add(-1);
						Log.d("NODATA","NODATA" + dbuserId.get(i));
					}

				}

			} catch (NullPointerException e) {
				// TODO: handle exception
				e.printStackTrace();
				Log.d("NULL", "NULL" + e);
			}
			 */


			address = dbutil.addressFunc();

			//Log.d("MATCHEDIDS", "MATCHEDIDS" + matched.size());

		}catch(SQLException se){

			se.printStackTrace();
		}
		catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}


		pinAdapter = new PinListAdapter(con, address, tempTitle, matched);
		pinList.setAdapter(pinAdapter);
		//Toast.makeText(con, "" + objid_title, Toast.LENGTH_LONG).show();

		//Log.d("CALCULATEDITEMS","CALCULATEDITEMS" + address.size());

		pinList.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1,
					int position, long arg3) {
				// TODO Auto-generated method stub
				try {

					Intent intent = new Intent(con,EditShowPin.class);
					intent.putExtra("objId", objectIdDb.get(position));
					startActivity(intent);
					finish();
					overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);

				}catch(IndexOutOfBoundsException ex){

					ex.printStackTrace();
				}
				catch (NullPointerException e) {
					// TODO: handle exception
					e.printStackTrace();
				}
				catch (Exception e) {
					// TODO: handle exception
				}
			}
		});

		//super.onPostExecute(result);

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_pin_list, menu);
		return false;
	}



	class Asynch extends AsyncTask<String, Integer, ArrayList<HashMap<String, Integer>> >{

		@Override
		protected ArrayList<HashMap<String, Integer>> doInBackground(
				String... params) {

			// TODO Auto-generated method stub

			/*
			ParseQuery query1 = new ParseQuery("Incident_SafeCity");
			//query.whereWithinKilometers("location", point, 1.0);
			//query1.whereEqualTo("category_id", "CQDKvxCo5i");

			try {

				objects_test = query1.find();
				for(int i=0;i<objects_test.size();i++){

					categoryId.add(objects_test.get(i).getString("category_id"));
					address.add(objects_test.get(i).getString("address"));
				}

			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			ParseQuery query2 = new ParseQuery("Category_SafeCity");
			//query.whereWithinKilometers("location", point, 1.0);

			for(int j=0;j<categoryId.size();j++){

				query2.whereEqualTo("objectId", categoryId.get(j));

				try {

					objects_test1 = query2.find();
					for(int i=0;i<objects_test1.size();i++){
						title.add(objects_test1.get(i).getString("title"));
					}

				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			 */

			/*
			ParseQuery query2 = new ParseQuery("Category_SafeCity");
			//query.whereWithinKilometers("location", point, 1.0);

			for(int j=0;j<categoryId.size();j++){

				query2.whereEqualTo("objectId", categoryId.get(j));

				try {

					objects_test1 = query2.find();
					for(int i=0;i<objects_test1.size();i++){
						title.add(objects_test1.get(i).getString("title"));
					}

				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

			 */

			return null;
		}

		@Override
		protected void onPostExecute(ArrayList<HashMap<String, Integer>> result) {
			// TODO Auto-generated method stub
			//prog.setVisibility(View.GONE);

		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			//prog.setVisibility(View.VISIBLE);
			super.onPreExecute();
		}


	}



	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();

		finish();
		overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
	}

	@Override
	protected void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
	}

	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
	}



}
