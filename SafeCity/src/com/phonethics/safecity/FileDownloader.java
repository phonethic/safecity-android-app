package com.phonethics.safecity;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.net.SocketException;
import java.net.URL;
import java.net.URLConnection;

import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.http.ConnectionClosedException;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.xml.sax.InputSource;



import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Environment;
import android.util.Log;
import android.widget.Toast;



public class FileDownloader {

	Context						context;
	String						url;
	String						fileName="New File";
	String						xml;
	String						directoryName="New Folder";
	String						filePath = "/sdcard/"+directoryName+"/"+fileName;
	File 						serialFile,tempDirectory;
	boolean						postExecute = false, preExecute= false, downloadNewFileAlways=true;
	DefaultHttpClient 			httpClient;
	HttpPost 					httpPost;
	HttpResponse				httpRes;
	HttpEntity					httpEnt;
	public static final int 	DIALOG_DOWNLOAD_PROGRESS = 0;
	private ProgressDialog 		mProgressDialog;
	//Schedule					schdl;


	/*
	 *  Constructor of FileDownloader class which expect Context.
	 */
	public FileDownloader(Context context){
		this.context 	= context;
		postExecute 	= false;
		preExecute		= true;
		directoryName 	= context.getString(R.string.app_name);
		fileName		= "New File.bin";
		filePath = "/sdcard/"+directoryName+"/"+fileName;
		
		//schdl = new Schedule();
	}

	/*
	 * check whether network is available or no. This will be checked through all network resources like wi-fi, gprs.
	 */
	public boolean isNetworkAvailable() {
		try
		{
			ConnectivityManager cm = (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
			NetworkInfo networkInfo = cm.getActiveNetworkInfo();
			// if no network is available networkInfo will be null
			// otherwise check if we are connected
			if (networkInfo != null && networkInfo.isConnected()) {
				return true;
			}
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return false;
	}

	/*
	 * Receives url from where file needs to be download
	 */
	public void downloadFrom(String url){
		this.url = url;
	}

	/*
	 * File Name to be set in External Storage
	 */
	public void setFileName(String fileName){
		this.fileName = fileName;
		serialFile = new File(Environment.getExternalStorageDirectory(),fileName);
		filePath = "/sdcard/"+directoryName+"/"+this.fileName;
	}

	public void setDirectoryName(String directoryName){
		this.directoryName = directoryName;
		tempDirectory = new File(Environment.getExternalStorageDirectory(), directoryName);
		if(!tempDirectory.exists()){
			tempDirectory.mkdirs();
		}
		filePath = "/sdcard/"+directoryName+"/"+fileName;
	}
	
	
	
	/*
	 * Set to true if you want to download new file everytime.
	 * 
	 */
	public void downloadAlways(boolean downloadNewFileAlways){
		this.downloadNewFileAlways = downloadNewFileAlways;
	}

	/*
	 * Checks whether File is already present or no
	 */

	public boolean isFileExists(String filename){
		String tempFile = "/sdcard/"+directoryName+"/"+filename;
		serialFile = new File(tempFile);
		try{
			if (serialFile.exists()) {
				return true;
			}
			else{
				return false;
			}
		}catch(Exception ex){
			ex.printStackTrace();
			return false;
		}
	}
	
	public void deleteFile(String fileToDelete){
		String tempFile = "/sdcard/"+directoryName+"/"+fileToDelete;
		serialFile = new File(tempFile);
		if(serialFile.exists()){
			serialFile.delete();
			Toast.makeText(context, "File Deleted", 0).show();
		}
	}

	/*
	 * Will call Asynch method everytime
	 */
	
	public void startDownload() {
		mProgressDialog = new ProgressDialog(context);
		mProgressDialog.setMessage("Please Wait..");
		mProgressDialog.setIndeterminate(false);
		mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
		mProgressDialog.setCancelable(true);
		MyAsyncTask download = new MyAsyncTask();
		download.execute(url);

	}





	private class MyAsyncTask extends AsyncTask<String, String, String>{
		@Override
		protected String doInBackground(String... params) {
			// TODO Auto-generated method stub

			int count;

			try {
				httpClient 	=	new DefaultHttpClient();
				httpPost 	= 	new HttpPost(url);

				httpRes		=	httpClient.execute(httpPost);

				httpEnt		=	httpRes.getEntity();
				xml 		=	EntityUtils.toString(httpEnt);

				DocumentBuilderFactory 	dbf	=	DocumentBuilderFactory.newInstance();
				dbf.newDocumentBuilder();
				InputSource		is	= new InputSource();
				is.setCharacterStream(new StringReader(xml));



				URL url = new URL(params[0]);
				URLConnection connection = url.openConnection();
				connection.connect();

				int lenghtOfFile = connection.getContentLength();

				// input stream to read file - with 8k buffer
				InputStream input = new BufferedInputStream(url.openStream(), 8192);

				// Output stream to write file
				try
				{
					/*
					 * creating directory to store xml 
					 */
					File myDirectory = new File(Environment.getExternalStorageDirectory(), directoryName);

					if(!myDirectory.exists()) {                                 
						myDirectory.mkdirs();
					}
				}catch(Exception ex)
				{
					ex.printStackTrace();
				}
				FileOutputStream output = new FileOutputStream(filePath);


				byte data[] = new byte[1024];
				long total = 0;

				while (   ((count = input.read(data)) != -1)) {


					// publishing the progress....
					// After this onProgressUpdate will be called
					/*publishProgress(""+(int)((total*100)/lenghtOfFile));*/

					// writing data to file
					total += count;
					/*publishProgress(""+(int)((total*100)/lenghtOfFile));*/
					output.write(data, 0, count);
				}

				output.flush();
				// closing streams
				output.close();
				input.close();


			}catch(SocketException socketException){
				Log.d("***", "Socket Exception");

			}catch(ConnectionClosedException con){

			}catch(ConnectTimeoutException timeOut){
				Log.d("***", "connection time out");

			}catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			catch (Exception e){
				Log.e("Error: ", " "+e.getMessage());
			}


			return null;
		}

		@Override
		protected void onCancelled() {
			// TODO Auto-generated method stub
			super.onCancelled();
		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			 mProgressDialog.dismiss();
			postExecute = true;
			//new Schedule().bttn.performClick();
			
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			mProgressDialog.show();
			preExecute = true;
			/*new Schedule().getJsonData();*/

		}
	}


	public boolean isDownloadComplete(){
		return postExecute;
	}

	public boolean isDownloadStart() {
		return preExecute;

	}

}


