package com.phonethics.safecity;


import android.os.Bundle;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.Menu;

public class SplashScreen extends Activity {
	
	private static long SLEEP_TIME = 2;    // Sleep for some time
	Context context;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_splash_screen);
		

		context=this;
		IntentLauncher launcher = new IntentLauncher();
		launcher.start();
		
		
	}
	
	private class IntentLauncher extends Thread {
		@Override
		/**
		 * Sleep for some time and than start new activity.
		 */
		public void run() {
			try {
				// Sleeping
				Thread.sleep(SLEEP_TIME*1000);
			} catch (Exception e) {

			}

			// Start main activity
			Intent intent=new Intent(context, SafeCityHome.class);
			startActivity(intent);
			finish();
			//overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);

		}
	}	

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_splash_screen, menu);
		return true;
	}

}
