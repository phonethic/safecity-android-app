package com.phonethics.safecity;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.net.SocketException;
import java.net.URL;
import java.net.URLConnection;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashMap;

import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.http.ConnectionClosedException;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.xml.sax.InputSource;


import com.flurry.android.FlurryAgent;
import com.parse.ParseException;

import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

public class News extends Activity {



	String									URL="http://safecityapp.com/api/get_category_posts/?slug=news&dev=1";
	File 									serialFile = new File(Environment.getExternalStorageDirectory(),"abc.bin");
	Context									context;
	JSONObject								jsonObj,tempJsonObj;
	JSONArray								jsonArr;
	ArrayList<String> 						content,thumbnail,title;
	ListView								listView;
	Asynch									download;
	ArrayAdapterList aryList;
	Activity							actContxt;

	ProgressBar prog;

	FileDownloader downloader;

	DefaultHttpClient 			httpClient;
	HttpPost 					httpPost;
	HttpResponse				httpRes;
	HttpEntity					httpEnt;

	String url,xml;

	private ProgressDialog pDialog;
	public static final int progress_bar_type = 0; 

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_news);

		context 	= this;
		actContxt	= this;

		downloader = new FileDownloader(context);

		prog = (ProgressBar) findViewById(R.id.progNews);

		content 		= new ArrayList<String>();
		thumbnail 		= new ArrayList<String>();
		title 			= new ArrayList<String>();
		listView		= (ListView) findViewById(R.id.newsList);
		downloader.setDirectoryName("safecity");
		downloader.setFileName("abc.bin");

		if(downloader.isNetworkAvailable()){
			if(!downloader.isFileExists("abc.bin")){

				download		= new Asynch();
				download.execute(URL);

			}else{

				parseJson();

			}

		}else{

			if(downloader.isFileExists("abc.bin")){
				parseJson();

			}else{

				Toast.makeText(context, "No internet connection", 0).show();

			}
		}

		/*download		= new Asynch();
		download.execute(URL);*/



		listView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position,
					long id) {
				// TODO Auto-generated method stub

				FlurryAgent.logEvent("SafeCity_NewsDetail_Event");

				Intent intent = new Intent(context, WebViewActivity.class);
				intent.putExtra("content", content.get(position));
				intent.putExtra("title", title.get(position));
				startActivity(intent);
				overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);

			}
		});



	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_news, menu);
		return false;
	}



	class Asynch extends AsyncTask<String, Integer, ArrayList<HashMap<String, Integer>> >{

		@Override
		protected ArrayList<HashMap<String, Integer>> doInBackground(
				String... params) {

			// TODO Auto-generated method stub
			/*try {

				jsonObj 	= JSONfunctions.getJSONfromURL(params[0]);
				jsonArr	=	jsonObj.getJSONArray("posts");

				for(int i=0; i <jsonArr.length(); i++){

					tempJsonObj = jsonArr.getJSONObject(i);
					String tempTitle = tempJsonObj.getString("title");
					title.add(tempTitle);
					String tempContent = tempJsonObj.getString("content");
					content.add(tempContent);
				}
			} catch(JSONException e){
				Log.e("log_tag", "Error parsing data "+e.toString());
			} catch (Exception e){
				Log.e("Error: ", " "+e.getMessage());
			}
			 */

			//return null;

			int count;

			try {
				httpClient 	=	new DefaultHttpClient();
				httpPost 	= 	new HttpPost(URL);

				httpRes		=	httpClient.execute(httpPost);

				httpEnt		=	httpRes.getEntity();
				xml 		=	EntityUtils.toString(httpEnt);

				DocumentBuilderFactory 	dbf	=	DocumentBuilderFactory.newInstance();
				dbf.newDocumentBuilder();
				InputSource		is	= new InputSource();
				is.setCharacterStream(new StringReader(xml));



				URL url = new URL(params[0]);
				URLConnection connection = url.openConnection();
				connection.connect();

				int lenghtOfFile = connection.getContentLength();

				// input stream to read file - with 8k buffer
				InputStream input = new BufferedInputStream(url.openStream(), 8192);

				// Output stream to write file
				try
				{



					File myDirectory = new File(Environment.getExternalStorageDirectory(), "safecity");

					if(!myDirectory.exists()) {                                 
						myDirectory.mkdirs();
					}
				}catch(Exception ex)
				{
					ex.printStackTrace();
				}
				FileOutputStream output = new FileOutputStream("/sdcard/safecity/abc.bin");


				byte data[] = new byte[1024];
				long total = 0;

				while (   ((count = input.read(data)) != -1)) {


					// publishing the progress....
					// After this onProgressUpdate will be called
					/*publishProgress(""+(int)((total*100)/lenghtOfFile));*/

					// writing data to file
					total += count;
					/*publishProgress(""+(int)((total*100)/lenghtOfFile));*/
					output.write(data, 0, count);
				}

				output.flush();
				// closing streams
				output.close();
				input.close();


			}catch(SocketException socketException){
				Log.d("***", "Socket Exception");

			}catch(ConnectionClosedException con){

			}catch(ConnectTimeoutException timeOut){
				Log.d("***", "connection time out");

			}catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			catch (Exception e){
				Log.e("Error: ", " "+e.getMessage());
			}


			return null;

		}

		@Override
		protected void onPostExecute(ArrayList<HashMap<String, Integer>> result) {
			// TODO Auto-generated method stub

			//ArrayAdapter<String> adapter = new ArrayAdapter<String>(context, android.R.layout.simple_list_item_1, title);
			//removeDialog(progress_bar_type);
			parseJson();
			prog.setVisibility(View.GONE);

			//	Toast.makeText(context, title.get(0), 0).show();
			super.onPostExecute(result);
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			//showDialog(progress_bar_type);
			prog.setVisibility(View.VISIBLE);
			super.onPreExecute();
		}


	}

	@Override
	protected Dialog onCreateDialog(int id) {
		switch (id) {
		case progress_bar_type:
			pDialog = new ProgressDialog(this);
			pDialog.setIndeterminate(false);
			pDialog.setMessage("Please wait");
			pDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
			pDialog.setCancelable(true);
			pDialog.show();
			return pDialog;
		default:
			return null;
		}
	}


	public void parseJson(){
		try {

			File dir = Environment.getExternalStorageDirectory();
			File jsonFile = new File("/sdcard/safecity/abc.bin");
			FileInputStream stream = new FileInputStream(jsonFile);
			String jString = null;
			try {
				FileChannel fc = stream.getChannel();
				MappedByteBuffer bb = fc.map(FileChannel.MapMode.READ_ONLY, 0, fc.size());
				/* Instead of using default, pass in a decoder. */
				jString = Charset.defaultCharset().decode(bb).toString();
			}
			finally {
				stream.close();
			}

			/*JSONObject jsonObj =  new JSONObject(jString);
			jsonArr	=	jsonObj.getJSONArray("posts");

			for(int i=0; i <jsonArr.length(); i++){

				tempJsonObj = jsonArr.getJSONObject(i);
				String tempTitle = tempJsonObj.getString("title");
				title.add(tempTitle);
				String tempContent = tempJsonObj.getString("content");
				content.add(tempContent);


			}*/


			try {

				jsonObj =  new JSONObject(jString);
				jsonArr	=	jsonObj.getJSONArray("posts");

				for(int i=0; i <jsonArr.length(); i++){

					tempJsonObj = jsonArr.getJSONObject(i);
					String tempTitle_sub = tempJsonObj.getString("title");

					String chng = "&#8217;";

					String 	tempTitle = tempTitle_sub.replace(chng, "'");

					title.add(tempTitle);
					String tempContent = tempJsonObj.getString("content");
					content.add(tempContent);
				}
			} catch(JSONException e){
				Log.e("log_tag", "Error parsing data "+e.toString());
			} catch (Exception e){
				Log.e("Error: ", " "+e.getMessage());
			}


		}catch(Exception ex){

			ex.printStackTrace();
		}

		aryList = new ArrayAdapterList(actContxt, title);
		listView.setAdapter(aryList); 


	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();

		finish();
		overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);

	}

	@Override
	protected void onStart() {
		// TODO Auto-generated method stub
		super.onStart();

		FlurryAgent.onStartSession(this, "XYCF767S2WQJ6TR2VR2B");

	}

	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		super.onStop();

		FlurryAgent.onEndSession(this);

	}



}
