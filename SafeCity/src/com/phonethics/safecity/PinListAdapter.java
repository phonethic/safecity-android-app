package com.phonethics.safecity;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class PinListAdapter extends ArrayAdapter<String>{

	

	private Activity context;
	private static LayoutInflater inflator = null;
	private  ArrayList<String> data;
	private  ArrayList<String> values;
	private  ArrayList<String> title;
	private  ArrayList<Integer> matchedPos;

	
	public PinListAdapter(Activity context, ArrayList<String> data,  ArrayList<String> title, ArrayList<Integer> matchedPos){
		super(context,R.layout.listdata, data);
		this.context = context;
		this.data = data;
		this.title = title;
		this.matchedPos = matchedPos;
	}
		
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		
		View rowView = convertView;
		
		if(convertView == null){
			inflator = context.getLayoutInflater();
			rowView = inflator.inflate(R.layout.listdata, null);
			ViewHolder holder = new ViewHolder();
			
			holder.text = (TextView) rowView.findViewById(R.id.typeDesc);
			holder.title_txt = (TextView) rowView.findViewById(R.id.typeTitle);
			holder.arrow = (ImageView) rowView.findViewById(R.id.arrowImage);
			rowView.setTag(holder);
		}
		
		ViewHolder hold = (ViewHolder) rowView.getTag();
		hold.text.setText(data.get(position));
		hold.title_txt.setText(title.get(position));
		
		/*if(matchedPos.get(position)!=-1){
			
			hold.arrow.setVisibility(View.VISIBLE);
		}*/
		
		if(matchedPos.get(position) == position){
			
			Log.d("MATCHEDID ADAP","MATCHEDID  " + position);
			hold.arrow.setVisibility(View.VISIBLE);
		}
		else{
			
			hold.arrow.setVisibility(View.GONE);
		}
		
	/*	for(int i=0;i<matchedPos.size();i++){
			
			if(matchedPos.get(i) == position){
				
				hold.arrow.setVisibility(View.VISIBLE);
				Log.d("POSCHK","POSCHK" + i);
			}
		}*/
		/*Toast.makeText(context, "" + title.get(position), Toast.LENGTH_SHORT).show();*/
		
		return rowView;
	}

	static class ViewHolder{
		
		public TextView text;
		public TextView title_txt;
		public ImageView arrow;
		
	}
}

	