package com.phonethics.safecity;


import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.SQLException;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.InputType;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.flurry.android.FlurryAgent;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseGeoPoint;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;

public class DropPin extends Activity {

	int size;
	EditText lati,longi,address,dateEdit, timeEdit, desc;
	ImageView edit_addphoto;

	ParseGeoPoint point;
	Spinner list;

	InputMethodManager 		imm;
	Context context;

	private TimePicker timePicker1;
	private int year;
	private int month;
	private int day;
	private int hour;
	private int minute;

	ParseObject classObj, catObj, imageObj, incident_person;
	ParseFile parseFile;
	byte[] data = null;

	String path,add,description,category_put, username;
	String arr[],objId[];

	static final int DATE_DIALOG_ID = 999;
	static final int TIME_DIALOG_ID = 1;

	Bitmap thumbnail;
	//ImageView submit;
	TextView submit;

	Date date1;
	String idtoput;

	DbUtil db;
	ArrayList<String> catTitle, catObjId;

	ArrayList<Bitmap> multiImages;
	TextView spinner1;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_drop_pin);

		context = this;


		classObj = new ParseObject("Incident_SafeCity");

		catObj = new ParseObject("Category_SafeCity");

		imageObj = new ParseObject("Media");

		incident_person = new ParseObject("Incident_Person");

		db = new DbUtil(context);

		catTitle = new ArrayList<String>();

		catObjId = new ArrayList<String>();

		multiImages = new ArrayList<Bitmap>();

		ParseUser currentUser = ParseUser.getCurrentUser();
		if (currentUser != null) {

			username = currentUser.getObjectId();
			//Toast.makeText(context, "" + currentUser.getObjectId(), 0).show();
			// do stuff with the user
		} else {
			// show the signup or login screen
		}

		lati = (EditText) findViewById(R.id.edit_latitude);
		longi = (EditText) findViewById(R.id.edit_longitude);
		address = (EditText) findViewById(R.id.edit_address);
		dateEdit = (EditText) findViewById(R.id.edit_date);
		timeEdit = (EditText) findViewById(R.id.edit_time);
		desc = (EditText) findViewById(R.id.edit_decription);
		edit_addphoto = (ImageView) findViewById(R.id.edit_addphoto);
		submit = (TextView) findViewById(R.id.save);


		dateEdit.setCursorVisible(false);
		dateEdit.setInputType(InputType.TYPE_NULL);

		timeEdit.setCursorVisible(false);
		timeEdit.setInputType(InputType.TYPE_NULL);


		imm 			= (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
		this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

		list = (Spinner) findViewById(R.id.spinner);
		spinner1 = (TextView) findViewById(R.id.spinner1);

		context  = this;

		Calendar c = Calendar.getInstance();
		Date dt = c.getTime();
		year = c.get(Calendar.YEAR);
		month = c.get(Calendar.MONTH);
		day = c.get(Calendar.DAY_OF_MONTH);
		hour = c.get(Calendar.HOUR_OF_DAY);
		minute = c.get(Calendar.MINUTE);


		dateEdit.setText(new StringBuilder().append(month + 1).append("-").append(day).append("-").append(year));
		timeEdit.setText(new StringBuilder().append(pad(hour)).append(":").append(pad(minute)));

		imm.hideSoftInputFromWindow(desc.getWindowToken(), 0);

		Bundle bundle=getIntent().getExtras();

		if(bundle!=null){

			double cur_lati,cur_longi;
			String add;

			cur_lati = bundle.getDouble("current_latitude");
			cur_longi = bundle.getDouble("current_longitude");
			add = bundle.getString("current_address");

			lati.setText(cur_lati+"");
			longi.setText(cur_longi+"");
			address.setText(add);
			
			lati.setEnabled(false);
			longi.setEnabled(false);

			point = new ParseGeoPoint(cur_lati, cur_longi);
		}
		
		/*lati.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				Intent intent=new Intent(getApplicationContext(),EditLatLng.class);
				startActivity(intent);
				overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
			}
		});*/

		desc.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				desc.setCursorVisible(true);
				/*desc.setCursorVisible(true);
				desc.setFocusable(true);
				imm.showSoftInput(desc, 0);

				desc.postDelayed(new Runnable() {

					@Override
					public void run() {
						// TODO Auto-generated method stub
						InputMethodManager keyboard = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
						keyboard.showSoftInput(desc, keyboard.SHOW_IMPLICIT);

					}
				}, 200);
				 */
			}
		});

		desc.setOnFocusChangeListener(new View.OnFocusChangeListener() {
			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				if (hasFocus) {
					InputMethodManager keyboard = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
					keyboard.showSoftInput(desc, keyboard.SHOW_IMPLICIT);
				}
			}
		});


		edit_addphoto.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				AlertDialog.Builder  alertDialog = new AlertDialog.Builder(context);
				alertDialog.setIcon(R.drawable.ic_launcher);
				alertDialog.setTitle("SafeCity");
				alertDialog.setMessage("Choose Image From :");
				alertDialog.setIcon(R.drawable.ic_launcher);
				alertDialog.setCancelable(true);
				alertDialog.setPositiveButton("Gallery", new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						Intent intent = new Intent();
						intent.setType("image/*");
						intent.setAction(Intent.ACTION_GET_CONTENT);

						startActivityForResult(intent, 10);
						//overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
					}
				});
				alertDialog.setNegativeButton("Take Picture", new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

						startActivityForResult(takePictureIntent, 2);	
						//overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
					}
				});
				AlertDialog alert = alertDialog.create();
				alert.show();
			}
		});


		try {

			catTitle = db.catTitleFunc();
			catObjId = db.catObjIDFunc();

			spinner1.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub

					/*							categoryname.setVisibility(View.GONE);
					list.setVisibility(View.VISIBLE);*/
					list.performClick();
				}
			});


			list.setAdapter(new ArrayAdapter<String>(context, android.R.layout.simple_spinner_dropdown_item, catTitle));
			list.setOnItemSelectedListener(new OnItemSelectedListener() {

				@Override
				public void onItemSelected(AdapterView<?> parent, View view,
						int pos, long id) {
					// TODO Auto-generated method stub


					/*	Toast.makeText(parent.getContext(), 
							"OnItemSelectedListener : " + parent.getItemAtPosition(pos).toString() + catObjId.get(pos),
							Toast.LENGTH_SHORT).show();
					 */	 
					category_put  = catObjId.get(pos);
					spinner1.setText(catTitle.get(pos));

				}

				@Override
				public void onNothingSelected(AdapterView<?> arg0) {
					// TODO Auto-generated method stub

				}
			});


		}catch(SQLException sql){

			sql.printStackTrace();
		}
		catch (Exception e) {
			// TODO: handle exception

			e.printStackTrace();
		}


		/*ParseQuery query = new ParseQuery("Category_SafeCity");

		query.findInBackground(new FindCallback() {

			@Override
			public void done(List<ParseObject> objects, ParseException e) {
				// TODO Auto-generated method stub

				//objects.get(0).getObjectId();
				size =  objects.size();

				arr = new String[size];
				objId = new String[size];
				//	Toast.makeText(getApplicationContext(), " " + objects.size(), Toast.LENGTH_SHORT).show();

				//Toast.makeText(getApplicationContext(), " " + catObj.getObjectId(), Toast.LENGTH_SHORT).show();

				for(int i=0;i<objects.size();i++){

					arr[i] = objects.get(i).getString("title");

					objId[i] = objects.get(i).getObjectId();
				}



				for(int i=0;i<arr.length;i++){

					Log.d("---------------------->"," Drop Pin" + objId[i]);
				}

				list.setAdapter(new ArrayAdapter<String>(context, android.R.layout.simple_spinner_dropdown_item, arr));
				list.setOnItemSelectedListener(new OnItemSelectedListener() {

					@Override
					public void onItemSelected(AdapterView<?> parent, View view,
							int pos, long id) {
						// TODO Auto-generated method stub


						Toast.makeText(parent.getContext(), 
								"OnItemSelectedListener : " + parent.getItemAtPosition(pos).toString() + objId[pos],
								Toast.LENGTH_SHORT).show();

						category_put  = objId[pos];

					}

					@Override
					public void onNothingSelected(AdapterView<?> arg0) {
						// TODO Auto-generated method stub

					}
				});

			}
		});
		 */
		add = address.getText().toString();	

		//Date date = new Date();

		submit.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				/*Toast.makeText(context, date1.toString(), Toast.LENGTH_SHORT).show();
				Log.d("Check","--------------"+ date1.toString());*/

				//Toast.makeText(context, ""+multiImages.size(), Toast.LENGTH_SHORT).show();
				
				FlurryAgent.logEvent("Save_Pin_Event");

				description  = desc.getText().toString();


				SimpleDateFormat dateFormat = new SimpleDateFormat("MM-dd-yyyy HH:mm"); 
				try {
					date1 = dateFormat.parse(dateEdit.getText().toString()+" "+timeEdit.getText().toString());
					//Toast.makeText(context, date1.toString(), Toast.LENGTH_SHORT).show();

				} catch (java.text.ParseException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}


				classObj.put("location", point );
				classObj.put("address", add );	
				classObj.put("description", description + "");
				classObj.put("active", 1);
				classObj.put("mode", 5);
				classObj.put("verified", 1);
				classObj.put("category_id", category_put);
				classObj.put("date", date1 );
				classObj.put("user_id", username);
				classObj.saveInBackground(new SaveCallback() {

					@Override
					public void done(ParseException e) {
						// TODO Auto-generated method stub
						if (e == null) {

							idtoput = classObj.getObjectId();
							Log.d("Check","--------------"+ classObj.getObjectId());

							if(multiImages.size()==0){

								//Toast.makeText(context, "NO IMAGE", Toast.LENGTH_SHORT).show();

							}
							else {


								for(int i=0;i<multiImages.size();i++){


									Timestamp st = new Timestamp(date1.getTime());

									Log.d("TIMESTAMP","TIMESTAMP" + st);

									ByteArrayOutputStream stream = new ByteArrayOutputStream();

									multiImages.get(i).compress(Bitmap.CompressFormat.PNG, 0, stream);

									byte[] byteArray = stream.toByteArray();


									parseFile = new ParseFile("img"+i+".png",byteArray);

									parseFile.saveInBackground();

									ParseObject objctArr = new ParseObject("Media");


									objctArr.put("active", 1);
									objctArr.put("media_type", 1);
									objctArr.put("media_date", date1);
									objctArr.put("incident_id", idtoput);
									objctArr.put("media_link", parseFile);
									objctArr.saveInBackground();

									//ParseObject i = new ParseObject("media");

								}

								/*	parseFile = new ParseFile("img.jpg",data);

								parseFile.saveInBackground();

								imageObj.put("incident_id", idtoput);
								imageObj.put("media_link", parseFile);
								imageObj.saveInBackground();*/

							}


							incident_person.put("incident_id", idtoput);
							incident_person.saveInBackground();


						} else {

							e.printStackTrace();
						}
					}
				});



				Intent intent=new Intent(getApplicationContext(),SafeCityHome.class);
				startActivity(intent);
				finish();
				overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);

			}
		});


		dateEdit.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				imm.hideSoftInputFromWindow(dateEdit.getWindowToken(), 0);
				showDialog(DATE_DIALOG_ID);


			}
		});

		timeEdit.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				imm.hideSoftInputFromWindow(timeEdit.getWindowToken(), 0);
				showDialog(TIME_DIALOG_ID);

			}
		});
	}


	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub

		if(data == null || resultCode == 0){
			path="";
		}else if(requestCode == 2){

			/*layout.setBackgroundResource(R.drawable.published_page);*/
			thumbnail = (Bitmap) data.getExtras().get("data");
			final Uri contentUri = data.getData();
			path = getPath(contentUri);
			showImage(path);

		}

		else if(requestCode == 10){
			Uri selectedImage = data.getData();
			try {

				path = getRealPathFromURI(selectedImage);
				showImage(path);


			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

		super.onActivityResult(requestCode, resultCode, data);
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();

		Intent intent=new Intent(getApplicationContext(),SafeCityHome.class);
		startActivity(intent);
		finish();
		overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);

	}


	@Override
	protected Dialog onCreateDialog(int id) {
		switch (id) {
		case DATE_DIALOG_ID:
			// set date picker as current date
			return new DatePickerDialog(this, datePickerListener, year, month,day);

		case TIME_DIALOG_ID:
			// set time picker as current time
			return new TimePickerDialog(this, timePickerListener, hour, minute,false);

		}

		return null;
	}

	private DatePickerDialog.OnDateSetListener datePickerListener = new DatePickerDialog.OnDateSetListener() {

		// when dialog box is closed, below method will be called.
		public void onDateSet(DatePicker view, int selectedYear,
				int selectedMonth, int selectedDay) {
			year = selectedYear;
			month = selectedMonth;
			day = selectedDay;

			// set selected date into textview
			dateEdit.setText(new StringBuilder().append(month + 1).append("-").append(day).append("-").append(year));

			// set selected date into datepicker also
			/*			dpResult.init(year, month, day, null);
			 */
		}
	};


	private TimePickerDialog.OnTimeSetListener timePickerListener =   new TimePickerDialog.OnTimeSetListener() {
		public void onTimeSet(TimePicker view, int selectedHour,int selectedMinute) {
			hour = selectedHour;
			minute = selectedMinute;

			// set current time into textview
			timeEdit.setText(new StringBuilder().append(pad(hour)).append(":").append(pad(minute)));

			// set current time into timepicker
			/*timePicker1.setCurrentHour(hour);
			timePicker1.setCurrentMinute(minute);*/

		}
	};


	private static String pad(int c) {
		if (c >= 10)
			return String.valueOf(c);
		else
			return "0" + String.valueOf(c);
	}


	private void showImage(String receivedImage)   {
		if(!(path.equalsIgnoreCase("") )){
			Log.i("showImage","loading:"+path);
			//imagetext.setVisibility(View.GONE);
			BitmapFactory.Options bfOptions=new BitmapFactory.Options();
			bfOptions.inDither=false;                     //Disable Dithering mode
			bfOptions.inPurgeable=true;                   //Tell to gc that whether it needs free memory, the Bitmap can be cleared
			bfOptions.inInputShareable=true;			//Which kind of reference will be used to recover the Bitmap data after being clear, when it will be used in the future
			bfOptions.inSampleSize=4;

			bfOptions.inTempStorage=new byte[32 * 1024]; 


			File file=new File(path);

			//Toast.makeText(getApplicationContext(), " " + path, Toast.LENGTH_LONG).show();



			try{

				/*FileInputStream fis = new FileInputStream(file);
				Bitmap bi = BitmapFactory.decodeStream(fis);
				ByteArrayOutputStream baos = new ByteArrayOutputStream();
				bi.compress(Bitmap.CompressFormat.PNG, 0, baos);
				data = baos.toByteArray();
				 */
				BitmapFactory.Options options = new BitmapFactory.Options();
				options.inSampleSize = 2;
				Bitmap bm = BitmapFactory.decodeFile(path, options);

				options.inJustDecodeBounds = true;
				options.inPurgeable=true;
				options.inDither=false;
				options.inPreferredConfig = Bitmap.Config.RGB_565;


				//multiImages.add(bi);

				//Bitmap bmpToAdd = BitmapFactory.decodeByteArray(data, 0, data.length);

				multiImages.add(bm);

			}catch(Exception e){
				e.printStackTrace();

			}

			/*parseFile = new ParseFile("img2.jpg",data);

			parseFile.saveInBackground();
			 */
			FileInputStream fs=null;
			try {
				fs = new FileInputStream(file);
			} catch (FileNotFoundException e) {
				//TODO do something intelligent
				e.printStackTrace();
			}

			Bitmap bm = null;
			try {
				if(fs!=null) bm=BitmapFactory.decodeFileDescriptor(fs.getFD(), null, bfOptions);
			} catch (IOException e) {
				//TODO do something intelligent
				e.printStackTrace();
			} finally{ 
				if(fs!=null) {
					try {
						fs.close();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
			//bm=BitmapFactory.decodeFile(path, bfOptions); This one causes error: java.lang.OutOfMemoryError: bitmap size exceeds VM budget

			edit_addphoto.setImageBitmap(bm);
			//tabImage.setImageBitmap(bm);
			//bm.recycle();
			bm=null;
		}else{
			/*captureImage.setImageResource(R.drawable.capture_frame);
			tabImage.setImageResource(R.drawable.frame);
			imagetext.setVisibility(View.VISIBLE);*/
		}
	}


	private String getPath(Uri selectedImage) {
		// TODO Auto-generated method stub

		String[] projection = { MediaStore.Images.Media.DATA };
		Cursor cursor = getContentResolver().query(selectedImage, projection, null, null, null);
		int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
		cursor.moveToFirst();
		return cursor.getString(column_index);

	}


	public String getRealPathFromURI(Uri contentUri) {
		// can post image
		String [] proj={MediaStore.Images.Media.DATA};
		Cursor cursor = getContentResolver().query(contentUri,
				proj, // Which columns to return
				null, // WHERE clause; which rows to return (all rows)
				null, // WHERE clause selection arguments (none)
				null); // Order-by clause (ascending by name)
		int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
		cursor.moveToFirst();
		return cursor.getString(column_index);
	}


	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_drop_pin, menu);
		return false;
	}


	@Override
	protected void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		
		FlurryAgent.onStartSession(this, "XYCF767S2WQJ6TR2VR2B");
		
	}


	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		
		FlurryAgent.onEndSession(this);
	}
	
	

}
