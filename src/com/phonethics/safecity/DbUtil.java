package com.phonethics.safecity;

import java.util.ArrayList;

import android.R.integer;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import android.widget.Toast;

public class DbUtil {


	private static final int  DATABASE_VERSOIN=1;

	private DbHelper dbHelper;
	private final Context ourContext;

	private SQLiteDatabase sqlDataBase;
	/*
	 * Database Name
	 */
	private static final String DATABASE_NAME="SafeCity.db";

	/*
	 * Table Names
	 */
	private static final String DATABASE_TABLE_NAME="IncidentSafeCity";
	private static final String DATABASE_TABLE_CAT="CategorySafeCity";


	/*
	 * FIELD INITIALIZATION FOR IncidentSafecity Table
	 */
	public static final String KEY_OBJID="objectId";
	public static final String KEY_ACTIVE="active";
	public static final String Key_ADDRESS="address";
	public static final String KEY_CATID="categoryId";
	public static final String KEY_DATE="DATE";
	public static final String KEY_DESC="description";
	public static final String KEY_LAT="latitude";
	public static final String KEY_LON="longitude"; 
	public static final String KEY_MODE="mode"; 
	public static final String KEY_USER="userId"; 

	/*
	 * FIELD INITIALIZATION FOR CategorySafeCity Table
	 */

	public static final String KEY_CAT_OBJID="cat_objectId";
	public static final String KEY_CAT_COLOR="cat_color";
	public static final String KEY_CAT_POSITON="cat_position";
	public static final String KEY_CAT_TITLE="cat_title";
	public static final String KEY_CAT_VISIBLE="cat_visible";

	public DbUtil(Context context){

		ourContext = context;
	}

	private static class  DbHelper extends SQLiteOpenHelper
	{

		public DbHelper(Context context) {
			super(context, DATABASE_NAME, null, DATABASE_VERSOIN);

			// TODO Auto-generated constructor stub
		}

		@Override
		public void onCreate(SQLiteDatabase db) {
			// TODO Auto-generated method stub

			db.execSQL("CREATE TABLE "+
					DATABASE_TABLE_NAME +" ("+
					KEY_OBJID +" TEXT PRIMARY KEY, "+
					KEY_ACTIVE +" INTEGER, "+
					Key_ADDRESS+" TEXT, " +
					KEY_CATID +" TEXT, "+
					KEY_DATE +" INTEGER, "+
					KEY_DESC + " TEXT,"+
					KEY_LAT + " TEXT,"+
					KEY_LON + " TEXT, " +
					KEY_MODE + " INTEGER,"+
					KEY_USER + " TEXT);"
					);

			db.execSQL("CREATE TABLE " +
					DATABASE_TABLE_CAT +" ("+
					KEY_CAT_OBJID + " TEXT, "+
					KEY_CAT_COLOR +" TEXT, "+
					KEY_CAT_POSITON+ " INTEGER,"+
					KEY_CAT_TITLE+" TEXT,"+
					KEY_CAT_VISIBLE+" INTEGER);"
					);
		}

		@Override
		public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
			// TODO Auto-generated method stub


			db.execSQL("DROP TABLE IF EXISTS "+DATABASE_TABLE_NAME);
			db.execSQL("DROP TABLE IF EXISTS "+DATABASE_TABLE_CAT);

			onCreate(db);
		}
	}

	public  DbUtil open() throws SQLException
	{
		dbHelper=new DbHelper(ourContext);
		sqlDataBase=dbHelper.getWritableDatabase();

		return this;

	}

	public void close()
	{
		dbHelper.close();
	}


	/*
	 * Create entery for EXPENSES Table
	 */
	public long createEntery(String objid,int active,String address,String catid,long date,String desc,String lati,String longi,int mode,String userid) {
		// TODO Auto-generated method stub

		ContentValues cv=new ContentValues();
		cv.put(KEY_OBJID,objid);
		cv.put(KEY_ACTIVE,active);
		cv.put(Key_ADDRESS,address);
		cv.put(KEY_CATID,catid);
		cv.put(KEY_DATE,date);
		cv.put(KEY_DESC,desc);
		cv.put(KEY_LAT,lati);
		cv.put(KEY_LON,longi);
		cv.put(KEY_MODE,mode);
		cv.put(KEY_USER,userid);


		return sqlDataBase.insert(DATABASE_TABLE_NAME, null, cv);
	}

	public ArrayList<String> objectIdFunc()
	{
		ArrayList<String>  object_id =new ArrayList<String>();

		String query="SELECT "+KEY_OBJID+" FROM "+DATABASE_TABLE_NAME;

		open();
		Cursor  cursor = sqlDataBase.rawQuery(query,null);

		for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
		{
			object_id.add(cursor.getString(cursor.getColumnIndex(KEY_OBJID)));

		}

		cursor.close();
		close();
		return object_id;
	}


	public ArrayList<Integer> activeFunc()
	{
		ArrayList<Integer>  active_num =new ArrayList<Integer>();

		String query="SELECT "+KEY_ACTIVE+" FROM "+DATABASE_TABLE_NAME;

		open();
		Cursor  cursor = sqlDataBase.rawQuery(query,null);

		for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
		{
			active_num.add(cursor.getInt(cursor.getColumnIndex(KEY_ACTIVE)));

		}

		cursor.close();
		close();
		return active_num;
	}


	public ArrayList<String> addressFunc()
	{
		ArrayList<String>  addr =new ArrayList<String>();

		String query="SELECT "+Key_ADDRESS+" FROM "+DATABASE_TABLE_NAME;

		open();
		Cursor  cursor = sqlDataBase.rawQuery(query,null);

		for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
		{
			addr.add(cursor.getString(cursor.getColumnIndex(Key_ADDRESS)));

		}

		cursor.close();
		close();
		return addr;
	}

	public String addressFunc(String objId)
	{
		String addr  = "";

		String query="SELECT "+Key_ADDRESS+" FROM "+DATABASE_TABLE_NAME +" WHERE "+ KEY_OBJID+" = '"+ objId+"'";

		open();
		Cursor  cursor = sqlDataBase.rawQuery(query,null);

		for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
		{
			addr=cursor.getString(cursor.getColumnIndex(Key_ADDRESS));

		}

		cursor.close();
		close();
		return addr;
	}


	public ArrayList<String> catIdFunc()
	{
		ArrayList<String>  categoryID =new ArrayList<String>();

		String query="SELECT "+KEY_CATID+" FROM "+DATABASE_TABLE_NAME;

		open();
		Cursor  cursor = sqlDataBase.rawQuery(query,null);

		for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
		{
			categoryID.add(cursor.getString(cursor.getColumnIndex(KEY_CATID)));

		}

		cursor.close();
		close();
		return categoryID;
	}

	public String catIdFunc(String objId)
	{
		String catId  = "";

		String query="SELECT "+KEY_CATID+" FROM "+DATABASE_TABLE_NAME +" WHERE "+ KEY_OBJID+" = '"+ objId+"'";

		open();
		Cursor  cursor = sqlDataBase.rawQuery(query,null);

		for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
		{
			catId=cursor.getString(cursor.getColumnIndex(KEY_CATID));

		}

		cursor.close();
		close();
		return catId;
	}



	public ArrayList<Long> dateFunc()
	{
		ArrayList<Long>  date =new ArrayList<Long>();

		String query="SELECT "+KEY_DATE+" FROM "+DATABASE_TABLE_NAME;

		open();
		Cursor  cursor = sqlDataBase.rawQuery(query,null);

		for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
		{
			date.add(cursor.getLong(cursor.getColumnIndex(KEY_DATE)));

		}

		cursor.close();
		close();
		return date;
	}


	public long dateFunc(String objId)
	{
		long date = 0;

		String query="SELECT "+KEY_DATE+" FROM "+DATABASE_TABLE_NAME +" WHERE "+ KEY_OBJID+" = '"+ objId+"'";

		open();
		Cursor  cursor = sqlDataBase.rawQuery(query,null);

		for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
		{
			date =cursor.getLong(cursor.getColumnIndex(KEY_DATE));

		}

		cursor.close();
		close();
		return date;
	}



	public ArrayList<String> descFunc()
	{
		ArrayList<String>  description =new ArrayList<String>();

		String query="SELECT "+KEY_DESC+" FROM "+DATABASE_TABLE_NAME;

		open();
		Cursor  cursor = sqlDataBase.rawQuery(query,null);

		for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
		{
			description.add(cursor.getString(cursor.getColumnIndex(KEY_DESC)));

		}

		cursor.close();
		close();
		return description;
	}

	public String descFunc(String objId)
	{
		String desc  = "";

		String query="SELECT "+KEY_DESC+" FROM "+DATABASE_TABLE_NAME +" WHERE "+ KEY_OBJID+" = '"+ objId+"'";

		open();
		Cursor  cursor = sqlDataBase.rawQuery(query,null);

		for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
		{
			desc =cursor.getString(cursor.getColumnIndex(KEY_DESC));

		}

		cursor.close();
		close();
		return desc;
	}



	public ArrayList<String> latiFunc()
	{
		ArrayList<String>  latitude =new ArrayList<String>();

		String query="SELECT "+KEY_LAT+" FROM "+DATABASE_TABLE_NAME;

		open();
		Cursor  cursor = sqlDataBase.rawQuery(query,null);

		for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
		{
			latitude.add(cursor.getString(cursor.getColumnIndex(KEY_LAT)));

		}

		cursor.close();
		close();
		return latitude;
	}

	public String latiFunc(String objId)
	{
		String lat  = "";

		String query="SELECT "+KEY_LAT+" FROM "+DATABASE_TABLE_NAME +" WHERE "+ KEY_OBJID+" = '"+ objId+"'";

		open();
		Cursor  cursor = sqlDataBase.rawQuery(query,null);

		for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
		{
			lat=cursor.getString(cursor.getColumnIndex(KEY_LAT));

		}

		cursor.close();
		close();
		return lat;
	}



	public ArrayList<String> longiFunc()
	{
		ArrayList<String>  longitude =new ArrayList<String>();

		String query="SELECT "+KEY_LON+" FROM "+DATABASE_TABLE_NAME;

		open();
		Cursor  cursor = sqlDataBase.rawQuery(query,null);

		for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
		{
			longitude.add(cursor.getString(cursor.getColumnIndex(KEY_LON)));

		}

		cursor.close();
		close();
		return longitude;
	}


	public String longiFunc(String objId)
	{
		String longi  = "";

		String query="SELECT "+KEY_LON+" FROM "+DATABASE_TABLE_NAME +" WHERE "+ KEY_OBJID+" = '"+ objId+"'";

		open();
		Cursor  cursor = sqlDataBase.rawQuery(query,null);

		for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
		{
			longi = cursor.getString(cursor.getColumnIndex(KEY_LON));

		}

		cursor.close();
		close();
		return longi;
	}


	public ArrayList<Integer> modeFunc()
	{
		ArrayList<Integer>  mode =new ArrayList<Integer>();

		String query="SELECT "+KEY_MODE+" FROM "+DATABASE_TABLE_NAME;

		open();
		Cursor  cursor = sqlDataBase.rawQuery(query,null);

		for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
		{
			mode.add(cursor.getInt(cursor.getColumnIndex(KEY_MODE)));

		}

		cursor.close();
		close();
		return mode;
	}


	public ArrayList<String> userIdFunc()
	{
		ArrayList<String>  user_id =new ArrayList<String>();

		String query="SELECT "+KEY_USER+" FROM "+DATABASE_TABLE_NAME;

		open();
		Cursor  cursor = sqlDataBase.rawQuery(query,null);

		for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
		{
			user_id.add(cursor.getString(cursor.getColumnIndex(KEY_USER)));

		}

		cursor.close();
		close();
		return user_id;
	}

	public String userIdFunc(String objId)
	{
		String userId  = "";

		String query="SELECT "+KEY_USER+" FROM "+DATABASE_TABLE_NAME +" WHERE "+ KEY_OBJID+" = '"+ objId+"'";

		open();
		Cursor  cursor = sqlDataBase.rawQuery(query,null);

		for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
		{
			userId = cursor.getString(cursor.getColumnIndex(KEY_USER));

		}

		cursor.close();
		close();
		return userId;
	}


	public String userIdFuncByCatId(String catId)
	{
		String userId  = "";

		String query="SELECT "+KEY_USER+" FROM "+DATABASE_TABLE_NAME +" WHERE "+ KEY_CATID+" = '"+ catId+"'";

		open();
		Cursor  cursor = sqlDataBase.rawQuery(query,null);

		for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
		{
			userId = cursor.getString(cursor.getColumnIndex(KEY_USER));

		}

		cursor.close();
		close();
		return userId;
	}



	/*
	 *Create entries for Category Table 
	 */

	public long categoryEntry(String objid,String color,int pos,String title,int visible) {
		// TODO Auto-generated method stub

		ContentValues cv=new ContentValues();
		cv.put(KEY_CAT_OBJID,objid);
		cv.put(KEY_CAT_COLOR,color);
		cv.put(KEY_CAT_POSITON,pos);
		cv.put(KEY_CAT_TITLE,title);
		cv.put(KEY_CAT_VISIBLE,visible);

		/*
		db.execSQL("CREATE TABLE " +
				DATABASE_TABLE_CAT +" ("+
				KEY_CAT_OBJID + "TEXT PRIMARY KEY, "+
				KEY_CAT_COLOR +"TEXT, "+
				KEY_CAT_POSITON+ "INTEGER,"+
				KEY_CAT_TITLE+"TEXT,"+
				KEY_CAT_VISIBLE+"INTEGER);"
				);*/


		return sqlDataBase.insert(DATABASE_TABLE_CAT, null, cv);
	}

	public ArrayList<String> catObjIDFunc()
	{
		ArrayList<String>  catObjId =new ArrayList<String>();

		String query="SELECT "+KEY_CAT_OBJID+" FROM "+DATABASE_TABLE_CAT;

		open();
		Cursor  cursor = sqlDataBase.rawQuery(query,null);

		for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
		{
			catObjId.add(cursor.getString(cursor.getColumnIndex(KEY_CAT_OBJID)));

		}

		cursor.close();
		close();
		return catObjId;
	}

	public String catObjIDFunc(String title)
	{
		String  filterCatId = "";

		String query="SELECT "+KEY_CAT_OBJID+" FROM "+DATABASE_TABLE_CAT + " WHERE " + KEY_CAT_TITLE + " = '"+title+"'" ;

		open();
		Cursor  cursor = sqlDataBase.rawQuery(query,null);

		for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
		{
			filterCatId = cursor.getString(cursor.getColumnIndex(KEY_CAT_OBJID));

		}

		cursor.close();
		close();
		return filterCatId;
	}



	public ArrayList<String> catColorFunc()
	{
		ArrayList<String>  color =new ArrayList<String>();

		String query="SELECT "+KEY_CAT_COLOR+" FROM "+DATABASE_TABLE_CAT;

		open();
		Cursor  cursor = sqlDataBase.rawQuery(query,null);

		for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
		{
			color.add(cursor.getString(cursor.getColumnIndex(KEY_CAT_COLOR)));

		}

		cursor.close();
		close();
		return color;
	}


	public ArrayList<Integer> catPosFunc()
	{
		ArrayList<Integer>  postion =new ArrayList<Integer>();

		String query="SELECT "+KEY_CAT_POSITON+" FROM "+DATABASE_TABLE_CAT;

		open();
		Cursor  cursor = sqlDataBase.rawQuery(query,null);

		for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
		{
			postion.add(cursor.getInt(cursor.getColumnIndex(KEY_CAT_POSITON)));

		}

		cursor.close();
		close();
		return postion;
	}


	public ArrayList<String> catTitleFunc()
	{
		ArrayList<String>  title =new ArrayList<String>();

		String query="SELECT "+KEY_CAT_TITLE+" FROM "+DATABASE_TABLE_CAT;

		open();
		Cursor  cursor = sqlDataBase.rawQuery(query,null);

		for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
		{
			title.add(cursor.getString(cursor.getColumnIndex(KEY_CAT_TITLE)));

		}

		cursor.close();
		close();
		return title;
	}

	public String getcatTitleFunc(String objid)
	{
		String  title ="";

		String query="SELECT "+KEY_CAT_TITLE+" FROM "+DATABASE_TABLE_CAT+" categ INNER JOIN "+DATABASE_NAME+" incid ON categ."+KEY_CAT_OBJID+" = incid."+KEY_CATID + " WHERE "+KEY_CATID+" ='"+objid+"'";

		open();
		Cursor  cursor = sqlDataBase.rawQuery(query,null);

		for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
		{
			title=cursor.getString(cursor.getColumnIndex(KEY_CAT_TITLE));
		}

		cursor.close();
		close();
		return title;
	}



	public String catTitleFunc(String catId)
	{
		String  title = "";

		String query="SELECT "+KEY_CAT_TITLE+" FROM "+DATABASE_TABLE_CAT + " WHERE " + KEY_CAT_OBJID + " = '"+catId+"'" ;

		open();
		Cursor  cursor = sqlDataBase.rawQuery(query,null);

		for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
		{
			title = cursor.getString(cursor.getColumnIndex(KEY_CAT_TITLE));

		}

		cursor.close();
		close();
		return title;
	}


	public ArrayList<Integer> catVisibleFunc()
	{
		ArrayList<Integer>  visible =new ArrayList<Integer>();

		String query="SELECT "+KEY_CAT_VISIBLE+" FROM "+DATABASE_TABLE_CAT;

		open();
		Cursor  cursor = sqlDataBase.rawQuery(query,null);

		for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
		{
			visible.add(cursor.getInt(cursor.getColumnIndex(KEY_CAT_VISIBLE)));

		}

		cursor.close();
		close();
		return visible;
	}

	public void deleteTable()
	{

		try
		{
			open();
			sqlDataBase.delete(DATABASE_TABLE_NAME, null, null);
			close();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}


	public void deleteTableCategory()
	{

		try
		{
			open();
			sqlDataBase.delete(DATABASE_TABLE_CAT, null, null);
			close();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}


	public void deleteAllRecords()
	{

		try
		{
			open();

			Log.d("DELETE","DELETEQUERYCALLED===");
			//sqlDataBase.delete(DATABASE_TABLE_NAME, null, null);
			sqlDataBase.execSQL("delete * from "+ DATABASE_TABLE_NAME);
			close();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}

	public void delete_byID(String objId){

		try{

			sqlDataBase.delete(DATABASE_TABLE_NAME, KEY_OBJID + "='" + objId + "'", null);
			//sqlDataBase.delete(tableName, carId + "=" + carVal, null);
			Log.d("caVal","carVal " + objId);
			Log.d("Delete", "DATA DELETED SUCCEFULLY");
		}catch(Exception e){
			Log.e("DB ERROR", e.toString());
			e.printStackTrace();
		}
	}


	public void updateRecord(String objId, String description,String addChanged, String title){

		ContentValues cv = new ContentValues();

		try {

			cv.put(KEY_DESC, description);
			//	cv.put(KEY_CATID, catChangedId);
			cv.put(Key_ADDRESS, addChanged);
			//	cv.put(KEY_DATE, date);

			cv.put(KEY_CATID, title);

			sqlDataBase.update(DATABASE_TABLE_NAME, cv, KEY_OBJID + "='" + objId + "'", null);


		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

}

