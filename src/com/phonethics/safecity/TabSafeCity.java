package com.phonethics.safecity;

import com.flurry.android.FlurryAgent;

import android.net.Uri;
import android.os.Bundle;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

public class TabSafeCity extends Activity {

	LinearLayout videoLay, quesLay,phoneLay,msgLay, revLay, shrLay;
	RelativeLayout relative_aboutApp;
	String shareUrl;
	Context con;
	TextView text_androidapp;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_tab_safe_city);

		PackageInfo pInfo = null;
		try {
			pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
		} catch (NameNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String version = pInfo.versionName;

		con = this;

		videoLay = (LinearLayout) findViewById(R.id.linear_watchvideo);
		quesLay = (LinearLayout) findViewById(R.id.linear_how);
		phoneLay = (LinearLayout) findViewById(R.id.linear_contactNumber);
		msgLay = (LinearLayout) findViewById(R.id.linear_msg);
		revLay = (LinearLayout) findViewById(R.id.linear_review);
		shrLay = (LinearLayout) findViewById(R.id.linear_share);
		relative_aboutApp = (RelativeLayout) findViewById(R.id.relative_aboutApp);
		text_androidapp = (TextView) findViewById(R.id.text_androidapp);

		text_androidapp.setText("SafeCity Android Version " + version);

		shareUrl = " https://market.android.com/search?q=pname:com.phonethics.safecity";


		relative_aboutApp.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				FlurryAgent.logEvent("AboutApp_Description_Event");

				//Toast.makeText(con, "Clicked", 0).show();
				Intent intent = new Intent(con, WebViewActivity.class);
				intent.putExtra("aboutTab", 1);
				startActivity(intent);
				overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);

			}
		});

		videoLay.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub


				Intent intent = new Intent(Intent.ACTION_VIEW); 
				intent.setDataAndType(Uri.parse("http://safecityapp.com/wp-content/uploads/2013/03/safe-city-ios-app-demo.mp4"), "video/mp4");
				startActivity(intent);
				overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);


			}
		});

		quesLay.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				Intent intent = new Intent(con, Help.class);
				startActivity(intent);
				overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);

			}
		});

		phoneLay.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub


				Intent call = new Intent(android.content.Intent.ACTION_DIAL);
				call.setData(Uri.parse("tel: 912265160691"));
				startActivity(call);
				overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);

			}
		});

		msgLay.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				Intent email=new Intent(Intent.ACTION_SEND);

				email.putExtra("android.intent.extra.EMAIL",new String[]{getResources().getString(R.string.emailId)});
				email.putExtra("android.intent.extra.SUBJECT", "Ref: SafeCity" );
				email.putExtra("android.intent.extra.TEXT", "Enter your messsage here");
				email.setType("text/plain");
				startActivity(Intent.createChooser(email, "Send mail..."));
				overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);

			}
		});


		revLay.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=com.phonethics.safecity")));
				overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);

			}
		});


		shrLay.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub


				Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
				sharingIntent.setType("text/plain");
				sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "SafeCity");
				sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareUrl);
				startActivity(Intent.createChooser(sharingIntent, "Share via"));
				overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);

			}
		});

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_tab_safe_city, menu);
		return false;
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();

		finish();
		overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);

	}

	@Override
	protected void onStart() {
		// TODO Auto-generated method stub
		super.onStart();

		FlurryAgent.onStartSession(this, "XYCF767S2WQJ6TR2VR2B");
		 
	}

	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		super.onStop();

		FlurryAgent.onEndSession(this);
	}



}
