package com.phonethics.safecity;

import com.flurry.android.FlurryAgent;
import com.parse.ParseException;
import com.parse.ParseUser;
import com.parse.SaveCallback;

import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.hardware.input.InputManager;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class Settings extends Activity {

	ImageView img_list;
	LinearLayout msgLay, site;
	Context con;
	EditText firstName, lastName, phoneNo, emailId;

	String cur_user;
	InputMethodManager imm;
	//ParseUser currentUser;
	NetworkInfo netInfo;
	
	TextView doneBtn;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_settings);

		con = this;

		imm 			= (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
		this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);


		img_list = (ImageView) findViewById(R.id.img_list);
		doneBtn = (TextView) findViewById(R.id.doneImg);
		msgLay = (LinearLayout) findViewById(R.id.linear_msgLay);
		site = (LinearLayout) findViewById(R.id.linear_Site); 

		firstName = (EditText) findViewById(R.id.text_firstName);
		lastName = (EditText) findViewById(R.id.text_lastName);
		phoneNo = (EditText) findViewById(R.id.text_phone);
		emailId = (EditText) findViewById(R.id.text_email);


		ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		netInfo = cm.getActiveNetworkInfo();

		/*currentUser = ParseUser.getCurrentUser();

		currentUser.put("firstname", firstName.getText().toString());
		currentUser.put("lastname", lastName.getText().toString());
		currentUser.put("phone", phoneNo.getText().toString());
		currentUser.put("email", emailId.getText().toString());

		cur_user = currentUser.getObjectId();
		Toast.makeText(con, "" + cur_user, 0).show();*/

		/*if (currentUser != null) {
			// do stuff with the user

			cur_user = currentUser.getObjectId();
			Toast.makeText(con, "" + cur_user, 0).show();


		} else {
			// show the signup or login screen
		}
		 */
		img_list.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				finish();
				overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
			}
		});



		msgLay.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				Intent email=new Intent(Intent.ACTION_SEND);

				email.putExtra("android.intent.extra.EMAIL",new String[]{getResources().getString(R.string.emailId)});
				email.putExtra("android.intent.extra.SUBJECT", "Ref: SafeCity" );
				email.putExtra("android.intent.extra.TEXT", "Enter your messsage here");
				email.setType("text/plain");
				startActivity(Intent.createChooser(email, "Send mail..."));
				overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);

			}
		});

		site.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				FlurryAgent.logEvent("Phonethics_WebSite_Event");

				//Toast.makeText(getApplicationContext(), "clicked sit", 0).show();
				Intent intent = new Intent(con, WebViewActivity.class);
				intent.putExtra("aboutTab", 2);
				startActivity(intent);
				overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);

			}
		});


		doneBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				/*currentUser.saveInBackground(new SaveCallback() {

					@Override
					public void done(ParseException e) {
						// TODO Auto-generated method stub

						if(e!=null){

							Log.d("CHECK", "userupdates" + currentUser.getObjectId());
						}
						else{

							Log.d("CHECK", "userupdates" + e.getMessage());
						}
					}
				});*/

				doneBtn.setEnabled(false);

				if(netInfo != null && netInfo.isConnected()){



					ParseUser current = ParseUser.getCurrentUser();
					if (current != null) {
						// do stuff with the user
						current.put("firstname", firstName.getText().toString());
						current.put("lastname", lastName.getText().toString());
						current.put("phone", phoneNo.getText().toString());
						current.put("email", emailId.getText().toString());

						current.saveInBackground(new SaveCallback() {

							@Override
							public void done(ParseException e) {
								// TODO Auto-generated method stub

								if(e!=null){

									//								e.printStackTrace();

									String errorMsg = e.getMessage();

									/*char tmpFirst = Character.toUpperCase(errorMsg.charAt(0));

									String finalErrorMsg = errorMsg.replace(errorMsg.charAt(0), tmpFirst);
									 */
									char[] chArray = errorMsg.toCharArray();

									chArray[0] = Character.toUpperCase(chArray[0]);

									String finalErrorMsg = new String(chArray);

									AlertDialog.Builder alertDialogBuilder3 = new AlertDialog.Builder(con);
									alertDialogBuilder3.setTitle("Safe City");
									alertDialogBuilder3.setIcon(R.drawable.ic_launcher);
									alertDialogBuilder3
									.setMessage(finalErrorMsg)
									.setCancelable(false)
									.setPositiveButton("Ok",new DialogInterface.OnClickListener() {
										public void onClick(DialogInterface dialog,int id) {


										}
									})
									;

									AlertDialog alertDialog3 = alertDialogBuilder3.create();

									alertDialog3.show();

								}
								else{

									FlurryAgent.logEvent("Updating_User_Details_Event");

									Log.d("CURRENT","CURRENT -===- SUCCESS" );

									Toast.makeText(con, "User details saved successfully", 0).show();
									finish();
								}

								doneBtn.setEnabled(true);

							}
						});

					} else {
						// show the signup or login screen


					}


				}else{

					Toast.makeText(con, "No internet connection", 0).show();
				}

			}
		});

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.settings, menu);
		return false;
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();

		finish();
		overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
	}

	@Override
	protected void onStart() {
		// TODO Auto-generated method stub
		super.onStart();

		FlurryAgent.onStartSession(this, "XYCF767S2WQJ6TR2VR2B");

	}

	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		super.onStop();

		FlurryAgent.onEndSession(this);
	}



}
