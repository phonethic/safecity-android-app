package com.phonethics.safecity;


import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import org.json.JSONException;

import com.flurry.android.FlurryAgent;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnInfoWindowClickListener;
import com.google.android.gms.maps.GoogleMap.OnMarkerDragListener;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseGeoPoint;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SignUpCallback;
import com.phonethics.safecity.PinList.Asynch;
import com.sbstrm.appirater.Appirater;

import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings.Secure;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.SQLException;
import android.graphics.Color;
import android.support.v4.app.FragmentActivity;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.SlidingDrawer;
import android.widget.SlidingDrawer.OnDrawerCloseListener;
import android.widget.SlidingDrawer.OnDrawerOpenListener;
import android.widget.TextView;
import android.widget.Toast;

public class SafeCityHome extends FragmentActivity implements LocationListener, OnSeekBarChangeListener {

	RelativeLayout relLayoutTrans;
	ImageView addBtn, img_search, img_filter, img_locate, img_drop, tabNews, tabDrop, tabSafeCity, img_list, zoomBtn;

	Context con;
	SupportMapFragment fm;

	GoogleMap googleMap;
	String provider, current_address;

	Location location;

	double current_latitude, current_longitude;

	LatLng current_latlng;

	List<ParseObject> objects_test, objects_test1, catParseObj;
	ArrayList<String> address, categoryId, title, objects_incident, tempDb, localuserId, titleObjId, description, catTableObjId;
	ArrayList<Date> datetime;

	long tempDate;

	ParseGeoPoint current_point, matchedPoints[];

	Marker markerarr[];

	ArrayList<String> titleFromCatTable, colorFromCatTable;
	ArrayList<Integer> positionFromCatTable;

	Asynch as;
	private ProgressDialog pDialog;
	public static final int progress_bar_type = 0;

	DbUtil dbutil;

	ArrayList<Long> datechk ;

	Marker currentMarker;

	SlidingDrawer slider;
	ImageView listIcon, searchIcon, filterBtn;

	TextView doneBtn;

	EditText serachBox;
	LinearLayout searchLay;

	SeekBar bar;
	double radiusInKM = 1.0;
	int touch = 0;

	int totalPins;

	Appirater ap;
	ProgressBar progBar;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_safe_city_home);

		con = this;

		//ap.appLaunched(con);

		dbutil = new DbUtil(con);

		bar = (SeekBar) findViewById(R.id.bar);
		bar.setOnSeekBarChangeListener(this);

		progBar = (ProgressBar) findViewById(R.id.progbar);
		//listIcon = (ImageView) findViewById(R.id.img_list);

		//slider = (SlidingDrawer) findViewById(R.id.SlidingDrawer);
		img_list = (ImageView) findViewById(R.id.img_list);
		address = new ArrayList<String>();
		categoryId = new ArrayList<String>();
		title = new ArrayList<String>();
		objects_incident = new ArrayList<String>();
		tempDb = new ArrayList<String>();
		localuserId = new ArrayList<String>();
		titleObjId = new ArrayList<String>();
		description = new ArrayList<String>();

		datetime = new ArrayList<Date>();
		datechk = new ArrayList<Long>();

		//datechk = new ArrayList<String>();

		catTableObjId = new ArrayList<String>();
		titleFromCatTable = new ArrayList<String>();
		colorFromCatTable = new ArrayList<String>();
		positionFromCatTable = new ArrayList<Integer>();

		ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo netInfo = cm.getActiveNetworkInfo();



		/*slider.setOnDrawerOpenListener(new OnDrawerOpenListener() {
            @Override
            public void onDrawerOpened() {
               // slideButton.setBackgroundResource(R.drawable.closearrow);

            	Toast.makeText(con, "open", 0).show();
            }
        });

		 slider.setOnDrawerCloseListener(new OnDrawerCloseListener() {
	            @Override
	            public void onDrawerClosed() {
	               // slideButton.setBackgroundResource(R.drawable.openarrow);

	            	Toast.makeText(con, "closed", 0).show();
	            }
	        });*/

		String android_id = Secure.getString(getApplicationContext().getContentResolver(),Secure.ANDROID_ID);

		ParseQuery query = new ParseQuery("User");
		query.whereEqualTo("username", android_id);
		query.findInBackground(new FindCallback() {

			@Override
			public void done(List<ParseObject> objects, ParseException e) {
				// TODO Auto-generated method stub

				//for(int i=0;i<objects.size();i++)
				//Toast.makeText(con, "" + objects.get(i).getObjectId(), 0).show();
			}
		});

		if(netInfo!=null && netInfo.isConnected()){

			as  = new Asynch();
			as.execute();

		}
		else{

		}


		relLayoutTrans	= (RelativeLayout) findViewById(R.id.rel_layout2);
		addBtn = (ImageView) findViewById(R.id.img_add);

		img_drop			= (ImageView) findViewById(R.id.img_droppin);
		img_search			= (ImageView) findViewById(R.id.img_search);
		img_filter			= (ImageView) findViewById(R.id.img_filter);
		img_locate			= (ImageView) findViewById(R.id.img_locateme);

		tabNews 			= (ImageView) findViewById(R.id.tabNews);
		tabDrop				= (ImageView) findViewById(R.id.tabDrop);
		tabSafeCity 		= (ImageView) findViewById(R.id.tabSafeCity);

		serachBox = (EditText) findViewById(R.id.searchBox);
		searchIcon = (ImageView) findViewById(R.id.searchIcon);

		searchLay = (LinearLayout) findViewById(R.id.searchLayout);
		doneBtn = (TextView)  findViewById(R.id.img_done);

		zoomBtn = (ImageView) findViewById(R.id.zoomBtn);

		/*
		 * Code for getting current location starts from here
		 */


		/*ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo netInfo = cm.getActiveNetworkInfo();*/

		LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);



		fm = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);

		// Getting GoogleMap object from the fragment
		googleMap = fm.getMap();

		// Enabling MyLocation Layer of Google Map
		googleMap.setMyLocationEnabled(true);
		googleMap.setTrafficEnabled(true);

		if (netInfo != null && netInfo.isConnected()) {

			Criteria criteria = new Criteria();
			provider = locationManager.getBestProvider(criteria, false);
			location = locationManager.getLastKnownLocation(provider);

			if(location!=null){
				onLocationChanged(location);
			}
			else {

				AlertDialog.Builder alertDialogBuilder3 = new AlertDialog.Builder(con);
				alertDialogBuilder3.setTitle("Location Check");
				alertDialogBuilder3
				.setMessage("No location found. Please check GPS settings")
				.setIcon(R.drawable.ic_launcher)
				.setCancelable(false)
				.setPositiveButton("Ok",new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog,int id) {

						SafeCityHome.this.finish();
					}
				})
				;

				AlertDialog alertDialog3 = alertDialogBuilder3.create();

				alertDialog3.show();
			}
		}else{

			addBtn.setEnabled(false);
			tabDrop.setEnabled(false);	
			zoomBtn.setEnabled(false);

			AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(con);
			alertDialogBuilder.setTitle("Check Connection");
			alertDialogBuilder
			.setMessage("No internet connection do you want to exit ?")
			.setIcon(R.drawable.ic_launcher)
			.setCancelable(false)
			.setPositiveButton("Yes",new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog,int id) {

					SafeCityHome.this.finish();
				}
			})
			.setNegativeButton("No",new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog,int id) {
					// if this button is clicked, just close
					// the dialog box and do nothing
					dialog.cancel();
				}
			});

			AlertDialog alertDialog = alertDialogBuilder.create();

			alertDialog.show();
		}


		img_list.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				Intent intent=new Intent(getApplicationContext(),MenuList.class);
				startActivity(intent);
				finish();
				overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);

			}
		});

		addBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(relLayoutTrans.isShown()){

					relLayoutTrans.setVisibility(View.GONE);
					addBtn.setImageResource(R.drawable.add_button);

				}else{

					relLayoutTrans.setVisibility(View.VISIBLE);
					addBtn.setImageResource(R.drawable.delete_button);
				}

			}
		});


		img_drop.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				FlurryAgent.logEvent("Drop_Pin_Event");

				addBtn.setImageResource(R.drawable.add_button);
				relLayoutTrans.setVisibility(View.GONE);
				Intent intent=new Intent(getApplicationContext(),DropPin.class);
				intent.putExtra("current_address", current_address);
				intent.putExtra("current_latitude", current_latitude);
				intent.putExtra("current_longitude", current_longitude);
				startActivity(intent);
				finish();
				overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
			}
		});


		img_search.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				FlurryAgent.logEvent("MapViewSearch_Event");



				//addBtn.setImageResource(R.drawable.add_button);
				addBtn.setImageResource(R.drawable.add_button);
				relLayoutTrans.setVisibility(View.GONE);

				//serachBox.bringToFront();
				//	searchIcon.bringToFront();

				searchLay.setVisibility(View.VISIBLE);

				searchLay.bringToFront();

				img_list.setVisibility(View.INVISIBLE);

				doneBtn.setVisibility(View.VISIBLE);

				searchIcon.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub

						googleMap.clear();

						progBar.setVisibility(View.VISIBLE);


						InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
						imm.hideSoftInputFromWindow(serachBox.getWindowToken(), 0);

						String tosearch = serachBox.getText().toString();


						// Finds barbecue sauces that start with "Big Daddy's".
						ParseQuery query = new ParseQuery("Incident_SafeCity");
						//		query.whereStartsWith("address", tosearch);
						query.whereMatches("address", tosearch, "i");

						query.findInBackground(new FindCallback() {
							public void done(List<ParseObject> List, ParseException e) {
								// commentList now has the comments for myPost

								progBar.setVisibility(View.GONE);
								//List.size();
								// Toast.makeText(con, "searched clicked " + List.size() , 0).show();

								dbutil.deleteTable();

								dbutil.open();

								for(int i=0;i<List.size();i++){

									Date d = List.get(i).getDate("date");

									long getDate = d.getTime();

									double filterLat = List.get(i).getParseGeoPoint("location").getLatitude();

									double filterLon = List.get(i).getParseGeoPoint("location").getLongitude();


									dbutil.createEntery(List.get(i).getObjectId(), 1,List.get(i).getString("address"), List.get(i).getString("category_id"), getDate, List.get(i).getString("description"), filterLat+"", filterLon +"", 5, List.get(i).getString("user_id"));

									LatLng allPins = new LatLng(filterLat, filterLon);

									String filterAddress = "";
									Geocoder geoCoder = new Geocoder(
											getBaseContext(), Locale.getDefault());
									try {
										List<Address> addresses = geoCoder.getFromLocation(
												filterLat, 
												filterLon, 1);

										if (addresses.size() > 0) {
											for (int index = 0; 
													index < addresses.get(0).getMaxAddressLineIndex(); index++)
												filterAddress += addresses.get(0).getAddressLine(index) + " ";
										}
									}catch (IOException ex) {        
										ex.printStackTrace();
									}   


									Marker markerarr = googleMap.addMarker(new MarkerOptions()
									.position(allPins)
									.title(filterAddress));
									//.snippet("hw r u ?")
									//.icon(BitmapDescriptorFactory.fromResource(R.drawable.pin)));

									markerarr.setVisible(true);


									//createEntery(String objid,int active,String address,String catid,String date,String desc,String lati,String longi,int mode,String userid)*/
								}

								dbutil.close();
							}
						});

					}
				});


				doneBtn.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub

						doneBtn.setVisibility(View.INVISIBLE);
						img_list.setVisibility(View.VISIBLE);
						searchLay.setVisibility(View.GONE);

						/*Asynch as1 = new Asynch()
						as.execute();*/

					}
				});

				//serachBox.setImeActionLabel("Search",EditorInfo.IME_ACTION_UNSPECIFIED);

				/*serachBox.setOnEditorActionListener(new TextView.OnEditorActionListener() {

					@Override
					public boolean onEditorAction(TextView v, int actionId,
							KeyEvent event) {

						  if (actionId == EditorInfo.IME_ACTION_SEARCH) {

							  	Toast.makeText(con, "searching", 0).show();
					            return true;
					        }
					        return false;
					}
				});*/



				/*Intent intent=new Intent(getApplicationContext(),PinList.class);
				startActivity(intent);*/
			}
		});

		img_filter.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				FlurryAgent.logEvent("FilterBy_Event");

				addBtn.setImageResource(R.drawable.add_button);
				relLayoutTrans.setVisibility(View.GONE);
				//googleMap.clear();
				final ParseQuery queryForAll = new ParseQuery("Incident_SafeCity");

				relLayoutTrans.setVisibility(View.GONE);


				try {


					ArrayList<String> filterCatId = dbutil.catObjIDFunc();
					ArrayList<String> filterCatTitle = dbutil.catTitleFunc();



					filterCatTitle.add(0,"All");
					filterCatTitle.add("My Pins");

					//final CharSequence[] items = {"Foo", "Bar", "Baz"};

					final CharSequence[] cs = filterCatTitle.toArray(new CharSequence[filterCatTitle.size()]);

					AlertDialog.Builder builder = new AlertDialog.Builder(con);
					builder.setTitle("Make your selection");
					builder.setIcon(R.drawable.ic_launcher);
					builder.setItems(cs, new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int item) {
							// Do something with the selection

							//							Toast.makeText(con, cs[item] + " " + "is selected", 0).show();

							googleMap.clear();

							progBar.setVisibility(View.VISIBLE);

							if(cs[item].equals("All")){

								//ParseQuery queryForAll = new ParseQuery("Incident_SafeCity");
								/*queryForAll.findInBackground(new FindCallback() {

									@Override
									public void done(List<ParseObject> objects, ParseException e) {
										// TODO Auto-generated method stub

										Toast.makeText(con, " " + objects.size(), 0).show();
									}
								});
								 */

								queryForAll.whereWithinKilometers("location", current_point, radiusInKM);
							}
							else if(cs[item].equals("My Pins")){

								String username = null;

								ParseUser currentUser = ParseUser.getCurrentUser();
								if (currentUser != null) {

									username = currentUser.getObjectId();
									//Toast.makeText(context, "" + currentUser.getObjectId(), 0).show();
									// do stuff with the user
								} else {
									// show the signup or login screen
								}

								queryForAll.whereEqualTo("user_id", username);
								/*queryForAll.whereWithinKilometers("location", current_point, 1.0);*/


								/*								ParseQuery queryForAll = new ParseQuery("Incident_SafeCity");
								 */								/*queryForAll.findInBackground(new FindCallback() {

									@Override
									public void done(List<ParseObject> objects, ParseException e) {
										// TODO Auto-generated method stub

										Toast.makeText(con, " " + objects.size(), 0).show();


									}
								});
								  */							}
							else{

								final String filterCatIdIncident;
								filterCatIdIncident = dbutil.catObjIDFunc(cs[item].toString());
								queryForAll.whereEqualTo("category_id", filterCatIdIncident);
								queryForAll.whereWithinKilometers("location", current_point, radiusInKM);
								//queryForAll.whereEqualTo("title", cs[item]);
								//Toast.makeText(con, "ID IS " + dbutil.catObjIDFunc(cs[item].toString()), 0).show();


								/*ParseQuery queryForAll = new ParseQuery("Incident_SafeCity");*/

								/*
								queryForAll.findInBackground(new FindCallback() {

									@Override
									public void done(List<ParseObject> objects, ParseException e) {
										// TODO Auto-generated method stub

										Toast.makeText(con, " " + objects.size(), 0).show();

									}
								});

								 */							}

							queryForAll.findInBackground(new FindCallback() {


								@Override
								public void done(List<ParseObject> objects, ParseException e) {
									// TODO Auto-generated method stub

									//Toast.makeText(con, " " + objects.size(), 0).show();

									progBar.setVisibility(View.GONE);

									dbutil.deleteTable();

									dbutil.open();
									for(int i=0;i<objects.size();i++){

										Date d = objects.get(i).getDate("date");

										long getDate = d.getTime();

										double filterLat = objects.get(i).getParseGeoPoint("location").getLatitude();

										double filterLon = objects.get(i).getParseGeoPoint("location").getLongitude();


										dbutil.createEntery(objects.get(i).getObjectId(), 1,objects.get(i).getString("address"), objects.get(i).getString("category_id"), getDate, objects.get(i).getString("description"), filterLat+"", filterLon +"", 5, objects.get(i).getString("user_id"));

										LatLng allPins = new LatLng(filterLat, filterLon);

										String filterAddress = "";
										Geocoder geoCoder = new Geocoder(
												getBaseContext(), Locale.getDefault());
										try {
											List<Address> addresses = geoCoder.getFromLocation(
													filterLat, 
													filterLon, 1);

											if (addresses.size() > 0) {
												for (int index = 0; 
														index < addresses.get(0).getMaxAddressLineIndex(); index++)
													filterAddress += addresses.get(0).getAddressLine(index) + " ";
											}
										}catch (IOException ex) {        
											ex.printStackTrace();
										}   


										Marker markerarr = googleMap.addMarker(new MarkerOptions()
										.position(allPins)
										.title(filterAddress));
										//.snippet("hw r u ?")
										//.icon(BitmapDescriptorFactory.fromResource(R.drawable.pin)));

										markerarr.setVisible(true);


										//createEntery(String objid,int active,String address,String catid,String date,String desc,String lati,String longi,int mode,String userid)*/
									}
									dbutil.close();
								}
							});

						}
					});
					AlertDialog alert = builder.create();
					alert.show();




				}catch(SQLException sql){

					sql.printStackTrace();
				}
				catch (Exception e) {
					// TODO: handle exception

					e.printStackTrace();
				}


			}
		});

		img_locate.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				addBtn.setImageResource(R.drawable.add_button);
				relLayoutTrans.setVisibility(View.GONE);
				onLocationChanged(location);


			}
		});


		/*
		 *  Clicks for tab bars
		 */


		tabNews.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				FlurryAgent.logEvent("NewsList_Tab_Event");
				startActivity(new Intent(con, News.class));
				overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
			}
		});

		tabDrop.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				FlurryAgent.logEvent("Drop_Pin_Event");

				Intent intent=new Intent(getApplicationContext(),DropPin.class);
				intent.putExtra("current_address", current_address);
				intent.putExtra("current_latitude", current_latitude);
				intent.putExtra("current_longitude", current_longitude);
				startActivity(intent);
				finish();
				overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
			}
		});

		tabSafeCity.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				FlurryAgent.logEvent("About_Tab_Event");

				Intent intent = new Intent(getApplicationContext(),TabSafeCity.class);
				startActivity(intent);
				overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
			}
		});


		zoomBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				touch++;
				//Toast.makeText(con, "clicked", 0).show();
				if(touch%2!=0){


					Animation animation = new TranslateAnimation(0, 0,-90, 0);
					animation.setDuration(500);
					animation.setFillAfter(true);
					bar.startAnimation(animation);
					bar.setVisibility(View.VISIBLE);
				}
				else{

					Animation animation = new TranslateAnimation(0, 0,0, -90);
					animation.setDuration(500);
					animation.setFillAfter(true);
					bar.startAnimation(animation);
					bar.setVisibility(View.GONE);
				}

			}
		});

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_safe_city_home, menu);
		return false;
	}

	@Override
	public void onLocationChanged(Location location) {
		// TODO Auto-generated method stub

		current_latitude = location.getLatitude();

		// Getting longitude of the current location
		current_longitude = location.getLongitude();

		current_point = new ParseGeoPoint(current_latitude, current_longitude);

		// Creating a LatLng object for the current location
		current_latlng = new LatLng(current_latitude, current_longitude);

		// Showing the current location in Google Map
		googleMap.moveCamera(CameraUpdateFactory.newLatLng(current_latlng));

		// Zoom in the Google Map
		googleMap.animateCamera(CameraUpdateFactory.zoomTo(13));



		// getting current address

		current_address = "";
		Geocoder geoCoder = new Geocoder(
				getBaseContext(), Locale.getDefault());
		try {
			List<Address> addresses = geoCoder.getFromLocation(
					current_latitude, 
					current_longitude, 1);

			if (addresses.size() > 0) {
				for (int index = 0; 
						index < addresses.get(0).getMaxAddressLineIndex(); index++)
					current_address += addresses.get(0).getAddressLine(index) + " ";
			}
		}catch (IOException e) {        
			e.printStackTrace();
		}   


		// draw marker

		currentMarker = googleMap.addMarker(new MarkerOptions()
		.position(current_latlng)
		.title("Current Position")
		.icon(BitmapDescriptorFactory.fromResource(R.drawable.pin)));

		currentMarker.setVisible(true);
		currentMarker.setDraggable(true);


		googleMap.setOnMarkerDragListener(new OnMarkerDragListener() {

			@Override
			public void onMarkerDragStart(Marker arg0) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onMarkerDragEnd(Marker arg0) {
				// TODO Auto-generated method stub

				current_latlng = currentMarker.getPosition();
				current_latitude = current_latlng.latitude;
				current_longitude = current_latlng.longitude;

				current_address = "";
				Geocoder geoCoder = new Geocoder(
						getBaseContext(), Locale.getDefault());
				try {
					List<Address> addresses = geoCoder.getFromLocation(
							current_latitude, 
							current_longitude, 1);

					if (addresses.size() > 0) {
						for (int index = 0; 
								index < addresses.get(0).getMaxAddressLineIndex(); index++)
							current_address += addresses.get(0).getAddressLine(index) + " ";
					}
				}catch (IOException e) {        
					e.printStackTrace();
				}   

			}

			@Override
			public void onMarkerDrag(Marker arg0) {
				// TODO Auto-generated method stub


			}
		});


	}

	@Override
	public void onProviderDisabled(String provider) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onProviderEnabled(String provider) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {
		// TODO Auto-generated method stub

	}


	/*
	 * Sample code can be deleted
	 */

	class Asynch extends AsyncTask<String, Integer, ArrayList<HashMap<String, Integer>> >{

		@Override
		protected ArrayList<HashMap<String, Integer>> doInBackground(
				String... params) {

			// TODO Auto-generated method stub


			ParseQuery query1 = new ParseQuery("Incident_SafeCity");

			query1.whereWithinKilometers("location", current_point, 1.0);
			//query1.whereEqualTo("category_id", "CQDKvxCo5i");

			try {

				objects_test = query1.find();
				matchedPoints = new ParseGeoPoint[objects_test.size()];
				for(int i=0;i<objects_test.size();i++){

					matchedPoints[i] = objects_test.get(i).getParseGeoPoint("location");
					categoryId.add(objects_test.get(i).getString("category_id"));
					address.add(objects_test.get(i).getString("address"));
					objects_incident.add(objects_test.get(i).getObjectId());
					localuserId.add(objects_test.get(i).getString("user_id"));
					description.add(objects_test.get(i).getString("description"));
					datetime.add(objects_test.get(i).getDate("date"));


					//SimpleDateFormat format2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

					tempDate = datetime.get(i).getTime();

					Log.d("TITLE", "GETDATE" + datetime.get(i).toString());

					datechk.add(tempDate);

				}

			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			/*	ParseQuery query2 = new ParseQuery("Category_SafeCity");
			//query2.whereWithinKilometers("location", current_point, 1.0);

			for(int j=0;j<categoryId.size();j++){

				query2.whereEqualTo("objectId", categoryId.get(j));

				try {

					objects_test1 = query2.find();
					for(int i=0;i<objects_test1.size();i++){

						title.add(objects_test1.get(i).getString("title"));
						titleObjId.add(objects_test1.get(i).getObjectId());
						//Log.d("TITLE", "GETTINTITLE" + objects_test1.get(i).getString("title"));

					}

				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			 */

			ParseQuery getCatTable = new ParseQuery("Category_SafeCity");

			try {

				catParseObj = getCatTable.find();

				for(int i=0;i<catParseObj.size();i++){

					catTableObjId.add(catParseObj.get(i).getObjectId());
					titleFromCatTable.add(catParseObj.get(i).getString("title"));
					colorFromCatTable.add(catParseObj.get(i).getString("color"));
					positionFromCatTable.add(catParseObj.get(i).getInt("position"));

					//Log.d("gettingCATOBJID", "gettingCATOBJID === +++" + catParseObj.get(i).getString("title"));
				}

			} catch (ParseException e) {
				// TODO: handle exception
				e.printStackTrace();
			}

			return null;
		}

		@Override
		protected void onPostExecute(ArrayList<HashMap<String, Integer>> result) {
			// TODO Auto-generated method stub
			//prog.setVisibility(View.GONE);

			/*pinAdapter = new PinListAdapter(con, address, title);
			pinList.setAdapter(pinAdapter);
			Toast.makeText(con, "" + objid_title, Toast.LENGTH_LONG).show();
			 */
			removeDialog(progress_bar_type);

			ap.appLaunched(con);
			//Toast.makeText(con, "" + objects_incident.size(), 0).show();
			try {



				dbutil.deleteTable();
				dbutil.deleteTableCategory();
				dbutil.open();

				for(int i=0;i<objects_incident.size();i++){

					double getLat = matchedPoints[i].getLatitude() ;

					double getLong =  matchedPoints[i].getLongitude();


					//Log.d("hI","categoryId--------" + objects_test.get(i).getString("category_id"));
					dbutil.createEntery(objects_incident.get(i), 1, address.get(i), categoryId.get(i), datechk.get(i), description.get(i), getLat+"", getLong +"", 5, localuserId.get(i));

					/*categoryEntry(String objid,String color,int pos,String title,int visible) */
					/*createEntery(String objid,int active,String address,String catid,String date,String desc,String lati,String longi,int mode,String userid)*/

				}

				for(int j=0;j<catTableObjId.size();j++){

					//Log.d("CHECKIT","CHECKIT" +  titleFromCatTable.get(j));
					dbutil.categoryEntry(catTableObjId.get(j), colorFromCatTable.get(j), positionFromCatTable.get(j), titleFromCatTable.get(j), 1);
				}

				dbutil.close();
				tempDb = dbutil.catTitleFunc();

				for(int i=0;i<tempDb.size();i++){

					//Log.d("getting", "Retrieved--------------" + tempDb.get(i));
				}


			}catch(SQLException sq){

				sq.printStackTrace();
			}
			catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}


			for(int i=0;i<matchedPoints.length;i++){

				double getLat = matchedPoints[i].getLatitude();

				double getLong =  matchedPoints[i].getLongitude();

				//Log.d("score", "Retrieved ------------------" + points[i].getLatitude() + " scores");

				String address = "";
				Geocoder geoCoder = new Geocoder(
						getBaseContext(), Locale.getDefault());
				try {
					List<Address> addresses = geoCoder.getFromLocation(
							getLat, 
							getLong, 1);

					if (addresses.size() > 0) {
						for (int index = 0; 
								index < addresses.get(0).getMaxAddressLineIndex(); index++)
							address += addresses.get(0).getAddressLine(index) + " ";
					}
				}catch (IOException exp) {        
					exp.printStackTrace();
				}   

				


				LatLng allPins = new LatLng(getLat, getLong);

				markerarr = new Marker[matchedPoints.length]; 

				markerarr[i] = googleMap.addMarker(new MarkerOptions()
				.position(allPins)
				.title(address));
				//.snippet("hw r u ?")
				//.icon(BitmapDescriptorFactory.fromResource(R.drawable.pin)));

				markerarr[i].setVisible(true);

				Log.d("TITLE", "GETTINTITLE" + address);
			}

			super.onPostExecute(result);
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			//prog.setVisibility(View.VISIBLE);
			showDialog(progress_bar_type);
			super.onPreExecute();
		}


	}

	@Override
	protected Dialog onCreateDialog(int id) {
		switch (id) {
		case progress_bar_type:
			pDialog = new ProgressDialog(this);
			pDialog.setIndeterminate(false);
			pDialog.setMessage("Searching for location");
			pDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
			pDialog.setCancelable(false);

			pDialog.show();
			return pDialog;
		default:
			return null;
		}
	}

	@Override
	public void onProgressChanged(SeekBar seekBar, int progress,
			boolean fromUser) {
		// TODO Auto-generated method stub

		radiusInKM = progress/1000;
		MapDrawer md = new MapDrawer(googleMap);
		md.drawCircle(current_latlng, progress);
		//bar.setBackgroundColor(Color.BLUE + progress);
		//Toast.makeText(con, "" + progress, 0).show();
	}

	@Override
	public void onStartTrackingTouch(SeekBar seekBar) {
		// TODO Auto-generated method stub


	}

	@Override
	public void onStopTrackingTouch(SeekBar seekBar) {
		// TODO Auto-generated method stub

		progBar.setVisibility(View.VISIBLE);


		ParseQuery querySeekBar = new ParseQuery("Incident_SafeCity");

		querySeekBar.whereWithinKilometers("location", current_point, radiusInKM);

		querySeekBar.findInBackground(new FindCallback() {

			@Override
			public void done(List<ParseObject> List, ParseException e) {
				// TODO Auto-generated method stub

				progBar.setVisibility(View.INVISIBLE);

				//Toast.makeText(con, "" + List.size(), 0).show();

				totalPins = List.size();
				dbutil.deleteTable();

				dbutil.open();

				for(int i=0;i<List.size();i++){

					Date d = List.get(i).getDate("date");

					long getDate = d.getTime();

					double filterLat = List.get(i).getParseGeoPoint("location").getLatitude();

					double filterLon = List.get(i).getParseGeoPoint("location").getLongitude();


					dbutil.createEntery(List.get(i).getObjectId(), 1,List.get(i).getString("address"), List.get(i).getString("category_id"), getDate, List.get(i).getString("description"), filterLat+"", filterLon +"", 5, List.get(i).getString("user_id"));

					LatLng allPins = new LatLng(filterLat, filterLon);

					String filterAddress = "";
					Geocoder geoCoder = new Geocoder(
							getBaseContext(), Locale.getDefault());
					try {
						List<Address> addresses = geoCoder.getFromLocation(
								filterLat, 
								filterLon, 1);

						if (addresses.size() > 0) {
							for (int index = 0; 
									index < addresses.get(0).getMaxAddressLineIndex(); index++)
								filterAddress += addresses.get(0).getAddressLine(index) + " ";
						}
					}catch (IOException ex) {        
						ex.printStackTrace();
					}catch (Exception e2) {
						// TODO: handle exception

						e2.printStackTrace();
					} 


					Marker markerarr = googleMap.addMarker(new MarkerOptions()
					.position(allPins)
					.title(filterAddress));


					markerarr.setVisible(true);


				}

				dbutil.close();


			}
		});

		/*googleMap.setOnInfoWindowClickListener(new OnInfoWindowClickListener() {

			@Override
			public void onInfoWindowClick(Marker arg0) {
				// TODO Auto-generated method stub

				//for(int k=0;k<totalPins;k++){

					Toast.makeText(con,  "clicked " + arg0.getId(),0).show();
				//}
			}
		});*/

	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();

		//pDialog.setCancelable(true);
		finish();
	}

	@Override
	protected void onStart() {
		// TODO Auto-generated method stub
		super.onStart();

		FlurryAgent.onStartSession(this, "XYCF767S2WQJ6TR2VR2B");
		FlurryAgent.logEvent("SafeCity_App_Started_Event");
	}

	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		super.onStop();

		FlurryAgent.onEndSession(this);
	}



}
