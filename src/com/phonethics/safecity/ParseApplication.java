package com.phonethics.safecity;


import com.parse.LogInCallback;
import com.parse.Parse;
import com.parse.ParseACL;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.PushService;
import com.parse.SignUpCallback;

import com.parse.ParseUser;

import android.app.Application;
import android.content.Context;
import android.provider.Settings.Secure;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.widget.Toast;

public class ParseApplication extends Application {

	String android_id;
	Context con;

	@Override
	public void onCreate() {
		super.onCreate();

		con = this;

		// Add your initialization code here
		Parse.initialize(this, "YpqFCRLYxWEFcOizNbJ027Dw0pzg01vmPUnBILRJ", "oAh7kWBv1GEUVwIAC6kE6DvS8Ptcol8WZcbFK66e");

		/*
		ParseObject testObject = new ParseObject("TestObject");
		testObject.put("foo", "bar");
		testObject.saveInBackground();
		 */		
		PushService.subscribe(this, "", SafeCityHome.class);
		PushService.setDefaultPushCallback(this, SafeCityHome.class);


		ParseUser.enableAutomaticUser();
		ParseACL defaultACL = new ParseACL();

		// If you would like all objects to be private by default, remove this line.
		defaultACL.setPublicReadAccess(true);

		ParseACL.setDefaultACL(defaultACL, true);


		//android_id = Secure.getString(getApplicationContext().getContentResolver(),Secure.ANDROID_ID);
		
		TelephonyManager telephonyManager = (TelephonyManager)getSystemService(con.TELEPHONY_SERVICE);
		android_id = telephonyManager.getDeviceId();

		ParseUser user = new ParseUser();
		user.setUsername(android_id);
		user.setPassword(android_id);
		//user.setEmail("email@example.com");

		// other fields can be set just like with ParseObject
		//user.put("phone", "650-253-0000");
		
		Log.d("CHECK", "uniqueId------======" + android_id);

		user.signUpInBackground(new SignUpCallback() {
			public void done(ParseException e) {
				if (e == null) {
					// Hooray! Let them use the app now.

				} else {

					Log.d("CHECK", "CHECK==========> " + e.getMessage());

					ParseUser.logInInBackground(android_id,android_id, new LogInCallback() {
						public void done(ParseUser user, ParseException e) {
							if (user != null) {

								Log.d("CHECK", "SUCCESS==========> ");
								Log.d("USERID", "USERID" + android_id);

							} else {

								Log.d("CHECK", "FAILED==========> " + e.getMessage());
								// Signup failed. Look at the ParseException to see what happened.
							}
						}
					});

				}
			}
		});

		/*
		ParseUser currentUser = ParseUser.getCurrentUser();
		if (currentUser != null) {
			// do stuff with the user

			Toast.makeText(getApplicationContext(), " " + currentUser, Toast.LENGTH_SHORT).show();

		} else {
			// show the signup or login screen

			Toast.makeText(getApplicationContext(), "Not signed", Toast.LENGTH_SHORT).show();

		}*/
	}

}

