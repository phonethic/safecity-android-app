package com.phonethics.safecity;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.GoogleMap.OnMarkerDragListener;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.parse.ParseGeoPoint;

import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.Menu;
import android.widget.Toast;

public class EditLatLng extends FragmentActivity implements LocationListener {

	Context con;
	SupportMapFragment fm;

	GoogleMap googleMap;
	String provider, current_address;

	Location location;

	double current_latitude, current_longitude;

	LatLng current_latlng;

	public static final String SHARED_PREFERENCES_NAME = "EDITPIN";
	
	SharedPreferences prefs;
	Editor editor ;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_edit_lat_lng);

		con = this;

		ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo netInfo = cm.getActiveNetworkInfo();

		LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);



		fm = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.editMap);

		// Getting GoogleMap object from the fragment
		googleMap = fm.getMap();

		// Enabling MyLocation Layer of Google Map
		googleMap.setMyLocationEnabled(true);
		googleMap.setTrafficEnabled(true);


		if (netInfo != null && netInfo.isConnected()) {

			Criteria criteria = new Criteria();
			provider = locationManager.getBestProvider(criteria, false);
			location = locationManager.getLastKnownLocation(provider);

			if(location!=null){
				onLocationChanged(location);
			}
			else {

				AlertDialog.Builder alertDialogBuilder3 = new AlertDialog.Builder(con);
				alertDialogBuilder3.setTitle("Location Check");
				alertDialogBuilder3
				.setMessage("No location found. Please check GPS Settings")
				.setIcon(R.drawable.ic_launcher)
				.setCancelable(false)
				.setPositiveButton("Ok",new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog,int id) {

						EditLatLng.this.finish();
					}
				})
				;

				AlertDialog alertDialog3 = alertDialogBuilder3.create();

				alertDialog3.show();
			}
		}else{


			AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(con);
			alertDialogBuilder.setTitle("Check Connection");
			alertDialogBuilder
			.setMessage("Please check your internet connection")
			.setIcon(R.drawable.ic_launcher)
			.setCancelable(false)
			.setPositiveButton("Ok",new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog,int id) {

					EditLatLng.this.finish();
				}
			});

			AlertDialog alertDialog = alertDialogBuilder.create();

			alertDialog.show();
		}


	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.edit_lat_lng, menu);
		return false;
	}

	@Override
	public void onLocationChanged(Location location) {
		// TODO Auto-generated method stub

		current_latitude = location.getLatitude();

		// Getting longitude of the current location
		current_longitude = location.getLongitude();


		// Creating a LatLng object for the current location
		current_latlng = new LatLng(current_latitude, current_longitude);

		// Showing the current location in Google Map
		googleMap.moveCamera(CameraUpdateFactory.newLatLng(current_latlng));

		// Zoom in the Google Map
		googleMap.animateCamera(CameraUpdateFactory.zoomTo(13));



		current_address = "";
		Geocoder geoCoder = new Geocoder(
				getBaseContext(), Locale.getDefault());
		try {
			List<Address> addresses = geoCoder.getFromLocation(
					current_latitude, 
					current_longitude, 1);

			if (addresses.size() > 0) {
				for (int index = 0; 
						index < addresses.get(0).getMaxAddressLineIndex(); index++)
					current_address += addresses.get(0).getAddressLine(index) + " ";
			}
		}catch (IOException e) {        
			e.printStackTrace();
		}   


		final Marker currentMarker = googleMap.addMarker(new MarkerOptions()
		.position(current_latlng)
		.title("Current Position")
		.icon(BitmapDescriptorFactory.fromResource(R.drawable.pin)));

		currentMarker.setVisible(true);
		currentMarker.setDraggable(true);




		googleMap.setOnMarkerDragListener(new OnMarkerDragListener() {

			@Override
			public void onMarkerDragStart(Marker arg0) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onMarkerDragEnd(Marker arg0) {
				// TODO Auto-generated method stub

				current_latlng = currentMarker.getPosition();
				current_latitude = current_latlng.latitude;
				current_longitude = current_latlng.longitude;

				current_address = "";
				Geocoder geoCoder = new Geocoder(
						getBaseContext(), Locale.getDefault());
				try {
					List<Address> addresses = geoCoder.getFromLocation(
							current_latitude, 
							current_longitude, 1);

					if (addresses.size() > 0) {
						for (int index = 0; 
								index < addresses.get(0).getMaxAddressLineIndex(); index++)
							current_address += addresses.get(0).getAddressLine(index) + " ";
					}
				}catch (IOException e) {        
					e.printStackTrace();
				}   

			}

			@Override
			public void onMarkerDrag(Marker arg0) {
				// TODO Auto-generated method stub


			}
		});
	}

	@Override
	public void onProviderDisabled(String provider) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onProviderEnabled(String provider) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
	/*	super.onBackPressed();*/

		//Toast.makeText(con, "" + current_latitude, 0).show();
		Log.d("lat","lat" + current_latitude + " " + current_longitude);

		Intent intent = new Intent();
		intent.putExtra("latitude", current_latitude+"");
		intent.putExtra("longitude", current_longitude+"");
		intent.putExtra("address", current_address);

		setResult(RESULT_OK, intent);
		finish();

		overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
	}



}
