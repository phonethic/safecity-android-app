package com.phonethics.safecity;

import java.util.ArrayList;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView.FindListener;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class ArrayAdapterList extends ArrayAdapter<String> {
	
	private Activity context;
	private static LayoutInflater inflator = null;
	private  ArrayList<String> data;
	private  ArrayList<String> values;
	private  ArrayList<String> title;

	
	public ArrayAdapterList(Activity context, ArrayList<String> title){
		super(context,R.layout.news_list);
		this.context = context;
		//this.data = data;
		this.title = title;
		
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		
		View rowView = convertView;
		
		if(convertView == null){
			inflator = context.getLayoutInflater();
			rowView = inflator.inflate(R.layout.news_list, null);
			ViewHolder holder = new ViewHolder();
			
			holder.text = (TextView) rowView.findViewById(R.id.text_data);
			//holder.title_txt = (TextView) rowView.findViewById(R.id.typeTitle);
			rowView.setTag(holder);
		}
		
		ViewHolder hold = (ViewHolder) rowView.getTag();
		hold.text.setText(title.get(position).toString());
		/*Toast.makeText(context, "-----------" + title.get(position), 0).show();*/
		//hold.title_txt.setText(title.get(position));
		/*Toast.makeText(context, "" + title.get(position), Toast.LENGTH_SHORT).show();*/
		
		return rowView;
	}

	static class ViewHolder{
		
		public TextView text;
		//public TextView title_txt;
		
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return title.size();
	}

	@Override
	public String getItem(int position) {
		// TODO Auto-generated method stub
		return title.get(position);
	}
}
