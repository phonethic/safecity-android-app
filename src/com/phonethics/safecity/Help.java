package com.phonethics.safecity;

import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;

public class Help extends Activity {

	ImageView addBtn, info, helpList;
	int count = 0;



	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_help);



		addBtn = (ImageView) findViewById(R.id.img_add);

		info = (ImageView) findViewById(R.id.info1);

		helpList = (ImageView) findViewById(R.id.img_list);


		helpList.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				finish();
				overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);

			}
		});

		addBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				count++;

				if(count%2!=0){

					addBtn.setImageResource(R.drawable.delete_button);
					info.setImageResource(R.drawable.info_screen2);
				}
				else{

					addBtn.setImageResource(R.drawable.add_button);
					info.setImageResource(R.drawable.info_screen);
				}
			}
		});

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.help, menu);
		return false;
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();

		finish();
		overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);

	}

}
