package com.phonethics.safecity;

import com.phonethics.safecity.WebViewActivity.MyWebViewClient;

import android.os.Bundle;
import android.app.Activity;
import android.graphics.Bitmap;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.webkit.WebSettings.PluginState;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

public class PrivacyPolicy extends Activity {


	WebView pri_policy;
	ProgressBar webLink;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_privacy_policy);


		pri_policy = (WebView) findViewById(R.id.pri_policy);
		webLink = (ProgressBar) findViewById(R.id.webLink);

		//pri_policy.setText("Information Collected. Describe the information you collect and how you plan to use it. This is critical for personally identifiable information you are collecting, such as telephone number and Social Security number"+"\n\n" +"Mandatory and Optional Data. Indicate whether the information being requested is mandatory or optional, and explain what the result of providing optional information will be."+"\n\n"+"Tracking and Logging Data. If you are requesting specific information for tracking purposes, include a statement on how that information is being logged and how that information is being used."+"\n\n"+"Sharing of Information. Indicate whether you provide any information to third parties, the names of those third parties, and how these entities will be using the information.");

		pri_policy.getSettings().setJavaScriptEnabled(true);
		pri_policy.getSettings().setPluginsEnabled(true);
		pri_policy.getSettings().setPluginState(PluginState.ON);
		pri_policy.getSettings().setAllowFileAccess(true);

		pri_policy.loadUrl("https://policy-portal.truste.com/core/privacy-policy/Phonethics-Mobile-Media/48bc6228-ed00-4f30-9cfb-f94a8180a80c");

		pri_policy.setWebViewClient(new MyWebViewClient());


	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();

		finish();
		overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.privacy_policy, menu);
		return false;
	}

	class MyWebViewClient extends WebViewClient {
		@Override
		// show the web page in webview but not in web browser
		public boolean shouldOverrideUrlLoading(WebView view, String url) {

			//Toast.makeText(getApplicationContext(), "url is" + url, Toast.LENGTH_SHORT).show();
			view.loadUrl(url);
			return true;
		}

		@Override
		public void onPageFinished(WebView view, String url) {
			// TODO Auto-generated method stub

			webLink.setVisibility(View.GONE);
			super.onPageFinished(view, url);

		}

		@Override
		public void onPageStarted(WebView view, String url, Bitmap favicon) {
			// TODO Auto-generated method stub

			webLink.setVisibility(View.VISIBLE);
			super.onPageStarted(view, url, favicon);


		}
	}



}
