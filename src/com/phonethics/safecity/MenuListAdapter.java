package com.phonethics.safecity;

import java.util.ArrayList;


import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class MenuListAdapter extends ArrayAdapter<String>{
	
	private Activity context;
	private  ArrayList<String> title;
	private  ArrayList<Integer> icon;
	private static LayoutInflater inflator = null;

	
	public MenuListAdapter(Activity context, ArrayList<String> title, ArrayList<Integer> icon){
		super(context,R.layout.menulayout);
		this.context = context;
		
		this.title = title;
		this.icon = icon;
		
	}

	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		
		View rowView = convertView;
		
		if(convertView == null){
			inflator = context.getLayoutInflater();
			rowView = inflator.inflate(R.layout.menulayout, null);
			ViewHolder holder = new ViewHolder();
			
			holder.text = (TextView) rowView.findViewById(R.id.menuTitle);
			holder.icon = (ImageView) rowView.findViewById(R.id.menuIcon);
			rowView.setTag(holder);
		}
		
		ViewHolder hold = (ViewHolder) rowView.getTag();
		hold.text.setText(title.get(position).toString());
		hold.icon.setImageResource(icon.get(position));
		
		return rowView;
	}

	static class ViewHolder{
		
		public TextView text;
		public ImageView icon;
		
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return title.size();
	}

	@Override
	public String getItem(int position) {
		// TODO Auto-generated method stub
		return title.get(position);
	}
	
}
