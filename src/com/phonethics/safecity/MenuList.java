package com.phonethics.safecity;

import java.util.ArrayList;

import com.flurry.android.FlurryAgent;

import android.os.Bundle;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.Toast;

public class MenuList extends Activity {

	ListView menuList;
	ArrayList<String> menus;
	ArrayList<Integer> menu_icons;
	MenuListAdapter menuAdap;
	Activity context;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_menu_list);

		context = this;

		menuList = (ListView) findViewById(R.id.menuList);

		menus = new ArrayList<String>();

		menu_icons = new ArrayList<Integer>();


		menus.add("Map");
		menus.add("List");
		menus.add("Settings");
		menus.add("Help");
		menus.add("Privacy Policy");

		menu_icons.add(R.drawable.slide_map_white);
		menu_icons.add(R.drawable.slide_list);
		menu_icons.add(R.drawable.slide_settings);
		menu_icons.add(R.drawable.slide_help_white);
		menu_icons.add(R.drawable.policy);

		menuAdap = new MenuListAdapter(context, menus, menu_icons);
		menuList.setAdapter(menuAdap);

		menuList.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int position,
					long arg3) {
				// TODO Auto-generated method stub

				if(position == 0){

					//Toast.makeText(getApplicationContext(), "MAp CLicked", 0).show();
					Intent intent=new Intent(getApplicationContext(),SafeCityHome.class);
					startActivity(intent);
					finish();
					overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);

				}
				else if(position == 1){

					FlurryAgent.logEvent("Incident_List");
					Intent intent=new Intent(getApplicationContext(),PinList.class);
					startActivity(intent);
					overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
					//finish();
				}
				else if(position == 2){

					FlurryAgent.logEvent("Settings_Tab_Event");
					//Toast.makeText(getApplicationContext(), "Settings clicked", 0).show();
					Intent intent=new Intent(getApplicationContext(),Settings.class);
					startActivity(intent);
					overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);

				}
				else if(position == 3){

					FlurryAgent.logEvent("Help_Tab_Event");
					//Toast.makeText(getApplicationContext(), "Help Clicked", 0).show();
					Intent intent=new Intent(getApplicationContext(),Help.class);
					startActivity(intent);
					overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
					//finish();

				}
				else if(position == 4){
					
					//Toast.makeText(getApplicationContext(), "show policy", 0).show();
					/*AlertDialog.Builder alertDialogBuilder3 = new AlertDialog.Builder(context);
					alertDialogBuilder3.setTitle("Privacy Policy");
					alertDialogBuilder3
					.setMessage("Information Collected. Describe the information you collect and how you plan to use it. This is critical for personally identifiable information you are collecting, such as telephone number and Social Security number,Mandatory and Optional Data. Indicate whether the information being requested is mandatory or optional, and explain what the result of providing optional information will be.Tracking and Logging Data. If you are requesting specific information for tracking purposes, include a statement on how that information is being logged and how that information is being used.Sharing of Information. Indicate whether you provide any information to third parties, the names of those third parties, and how these entities will be using the information.")
					.setIcon(R.drawable.ic_launcher)
					.setCancelable(false)
					.setPositiveButton("Ok",new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog,int id) {

							
						}
					})
					;

					AlertDialog alertDialog3 = alertDialogBuilder3.create();

					alertDialog3.show();*/
					
					Intent intent=new Intent(getApplicationContext(),PrivacyPolicy.class);
					startActivity(intent);
					overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
					
				}
			}
		});

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.menu_list, menu);
		return false;
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();

		Intent intent=new Intent(getApplicationContext(),SafeCityHome.class);
		startActivity(intent);
		finish();
		overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
	}

	@Override
	protected void onStart() {	
		// TODO Auto-generated method stub
		super.onStart();
		
		FlurryAgent.onStartSession(this, "XYCF767S2WQJ6TR2VR2B");
		
	}

	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		
		FlurryAgent.onEndSession(this);
	}



}
