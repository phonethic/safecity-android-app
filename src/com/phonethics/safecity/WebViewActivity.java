package com.phonethics.safecity;

import android.os.Bundle;
import android.app.Activity;
import android.graphics.Bitmap;
import android.view.Menu;
import android.view.View;
import android.webkit.WebSettings.PluginState;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import android.widget.TextView;

public class WebViewActivity extends Activity {

	WebView web;
	String content, title;
	int value;
	TextView img_header;
	ProgressBar webLink;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_web_view);

		img_header = (TextView) findViewById(R.id.img_header);
		webLink = (ProgressBar) findViewById(R.id.prog);

		value = 0;
		web = (WebView) findViewById(R.id.web);
		web.getSettings().setJavaScriptEnabled(true);
		web.getSettings().setPluginsEnabled(true);
		web.getSettings().setPluginState(PluginState.ON);
		web.getSettings().setAllowFileAccess(true);


		web.getSettings().setUserAgentString("Mozilla/5.0 (Linux; U; Android 2.0; en-us; Droid Build/ESD20) AppleWebKit/530.17 (KHTML, like Gecko) Version/4.0 Mobile Safari/530.17");


		Bundle bundle=getIntent().getExtras();

		web.setWebViewClient(new MyWebViewClient());


		if(bundle!=null){

			value = bundle.getInt("aboutTab");

			if(value == 1){

				img_header.setText("App Description");
				// for about app click
				String about_content = "Safe City is a handy tool to geographically mark and report a safety issue faced by women and children. You can quickly locate, indentify and pin the issue on the map of your city.<br> It is aimed at creating a social response system where this information is actively pursued in reaching out to the person in need and bring to the notice the issues and their causes so they can be addressed and monitored.";
				web.loadData("<!DOCTYPE html><head><style type=\"text/css\">h1{font-family:\"Times New Roman\";font-size:20px;width:300px;color:rgb(0,99,180);}</style></head><body><br><p><center><h1>SafeCity</h1></center></p><p><font face = \"Times New Roman\">"+about_content+"</font></p></body></html>","text/html; charset=UTF-8", null);
			}
			else if(value == 2){

				img_header.setText("Phonethics.in");
				//web.loadData("http://www.phonethics.in","text/html; charset=UTF-8", null);
				web.loadUrl("http://www.phonethics.in");

			}
			else{

				// for news data

				content	=	bundle.getString("content");
				title = bundle.getString("title");

				web.loadData("<!DOCTYPE html><head><meta charset=\"UTF-8\"/><meta name=\"viewport\" content=\"width=device-width\" />" +
						"<title>SafeCity</title><link rel=\"stylesheet\" type=\"text/css\" media=\"all\" href=\" http://safecityapp.com/wp-content/themes/twentytwelve/style.css\" />" +
						"<style type=\"text/css\">.recentcomments a{display:inline !important;padding:0 !important;margin:0 !important;}</style>" +
						"<style type=\"text/css\">p {padding:5px !important;margin:5px !important;}</style></head>" +
						"<p><center><U><H2>"+title +"</H2></U></center></p>"+"<body class=\"single single-post postid-1606 single-format-standard single-author singular two-column right-sidebar\"><div class=\"entry-content\">"+content+"</div></body></html>","text/html; charset=UTF-8", null);

			}

		}

	}



	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_web_view, menu);
		return false ;
	}


	class MyWebViewClient extends WebViewClient {
		@Override
		// show the web page in webview but not in web browser
		public boolean shouldOverrideUrlLoading(WebView view, String url) {

			//Toast.makeText(getApplicationContext(), "url is" + url, Toast.LENGTH_SHORT).show();
			view.loadUrl(url);
			return true;
		}

		@Override
		public void onPageFinished(WebView view, String url) {
			// TODO Auto-generated method stub

			webLink.setVisibility(View.GONE);
			super.onPageFinished(view, url);

		}

		@Override
		public void onPageStarted(WebView view, String url, Bitmap favicon) {
			// TODO Auto-generated method stub

			webLink.setVisibility(View.VISIBLE);
			super.onPageStarted(view, url, favicon);


		}
	}


	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();

		finish();
		overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);

	}



}
