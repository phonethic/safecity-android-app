package com.phonethics.safecity;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import com.flurry.android.FlurryAgent;
import com.parse.DeleteCallback;
import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.GetDataCallback;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseGeoPoint;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;

import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.SQLException;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v4.widget.SimpleCursorAdapter;
import android.text.InputType;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;
import android.widget.AdapterView.OnItemSelectedListener;

public class EditShowPin extends Activity {


	String tableAdd, latitude, longitude, description, tableUserId;
	String objId;
	String cat_incident;
	String cat_title;
	Context con;
	DbUtil db;

	String cur_user, path;
	TextView categoryname, img_header, text_addphoto,edit_latitude, edit_longitude;

	EditText editAdd, edit_decription, edit_date, edit_time;
	Button remove;

	long datetime;
	ImageView edit_addphoto, updatePin1, sharePin, edit_addphoto1;
	TextView updatePin;

	Date dt2;
	SimpleDateFormat sdFormat1 ;
	SimpleDateFormat sdFormat2;

	byte[] byteArray;

	/*
	 * (non-Javadoc)
	 * @see android.app.Activity#onCreate(android.os.Bundle)
	 * For update items
	 */

	Spinner list;
	String category_put;
	ArrayList<String> catTitle,catObjId;
	byte[] data = null;
	ParseFile parseFile;
	Bitmap thumbnail;
	/*	Bitmap bitmap;*/

	ArrayList<Bitmap> bitmap;

	InputMethodManager 		imm;
	static final int DATE_DIALOG_ID = 999;
	static final int TIME_DIALOG_ID = 1;

	private TimePicker timePicker1;
	private int year;
	private int month;
	private int day;
	private int hour;
	private int minute;

	Date date1;
	long tmpDate;

	RelativeLayout updateLay;
	LinearLayout dynamicImagesAdd;

	NetworkInfo netInfo;

	/*ImageView images[];*/
	int inc=0;
	String extStorageDirectory ;
	FileOutputStream outStream;
	File filesave;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_edit_show_pin);

		con = this;

		db = new DbUtil(con);

		extStorageDirectory = Environment.getExternalStorageDirectory().toString();
		filesave = new File(extStorageDirectory, "img.png");

		ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		netInfo = cm.getActiveNetworkInfo();


		updateLay = (RelativeLayout) findViewById(R.id.updateLay);
		dynamicImagesAdd = (LinearLayout) findViewById(R.id.imagesDynamic);

		list = (Spinner) findViewById(R.id.spinner);
		remove = (Button) findViewById(R.id.remove);
		updatePin = (TextView) findViewById(R.id.updatePin);
		sharePin = (ImageView) findViewById(R.id.sharePin);
		updatePin.setVisibility(View.GONE);

		//sharePin.setEnabled(false);

		bitmap = new ArrayList<Bitmap>();

		ParseUser currentUser = ParseUser.getCurrentUser();
		if (currentUser != null) {
			// do stuff with the user

			cur_user = currentUser.getObjectId();
			//Toast.makeText(con, "" + cur_user, 0).show();


		} else {
			// show the signup or login screen
		}

		categoryname = (TextView) findViewById(R.id.spinner1);
		editAdd = (EditText) findViewById(R.id.edit_address);
		edit_latitude = (TextView) findViewById(R.id.edit_latitude);
		edit_longitude = (TextView) findViewById(R.id.edit_longitude);
		edit_decription = (EditText) findViewById(R.id.edit_decription);
		//remove = (Button) findViewById(R.id.remove);
		img_header = (TextView) findViewById(R.id.img_header);
		edit_date = (EditText) findViewById(R.id.edit_date);
		edit_time = (EditText) findViewById(R.id.edit_time);
		edit_addphoto = (ImageView) findViewById(R.id.edit_addphoto);
		//edit_addphoto1 = (ImageView) findViewById(R.id.edit_addphoto1);
		text_addphoto = (TextView) findViewById(R.id.text_addphoto);

		Bundle bundle=getIntent().getExtras();

		if(bundle!= null){

			objId = bundle.getString("objId");
		}

		try {

			tableUserId = db.userIdFunc(objId);
			description = db.descFunc(objId);
			tableAdd = db.addressFunc(objId);
			latitude = db.latiFunc(objId);
			longitude = db.longiFunc(objId);
			cat_incident = db.catIdFunc(objId);
			cat_title = db.catTitleFunc(cat_incident);
			datetime = db.dateFunc(objId);

			/*Log.d("chk", "GETTIN_DATE" + datetime);*/

			/*			String str = "8/29/2011 11:16:12 AM";
			 */

			dt2 =new Date(datetime);
			sdFormat1 = new SimpleDateFormat("dd-MMM-yyyy");
			sdFormat2 = new SimpleDateFormat("hh:mm aa");

			Log.d("","FINAL_DATE" + " " + sdFormat2.format(dt2));


			/*
			 * for image 
			 */
			Log.d("","current incidentid" + objId);

			if (netInfo != null && netInfo.isConnected()) {


				ParseQuery query = new ParseQuery("Media");
				query.whereEqualTo("incident_id", objId);
				query.findInBackground(new FindCallback() {

					@Override
					public void done(List<ParseObject> objects, ParseException e) {
						// TODO Auto-generated method stub


						/*final ImageView images[] = new ImageView[objects.size()];

						final int totalSize = objects.size();*/

						for(inc=0;inc<objects.size();inc++)
						{
							Log.d("","MediaobjectId" + objects.get(inc).getObjectId());

							try {

								ParseObject mediaObj = objects.get(inc);

								ParseFile mediaFile = (ParseFile)mediaObj.get("media_link");

								/*	images[inc] = new ImageView(con);
								images[inc].setId(inc);*/

								/*if(mediaFile == null){

									Toast.makeText(con, "NO IMAGE", 0).show();
								}
								else{

									Toast.makeText(con, "IMAGE", 0).show();
								}
								 */


								Log.d("","1--Id" +inc);

								mediaFile.getDataInBackground(new GetDataCallback() {
									public void done(byte[] data, ParseException e) {

										if (e == null) {


											sharePin.setEnabled(true);

											Bitmap bitmapSingle = BitmapFactory.decodeByteArray(data, 0, data.length);
											bitmap.add(bitmapSingle);
											edit_addphoto.setVisibility(View.VISIBLE);
											edit_addphoto.setImageBitmap(bitmapSingle);

											/*extStorageDirectory = Environment.getExternalStorageDirectory().toString();*/
											/*filesave = new File(extStorageDirectory, "img.png");*/
											try {
												outStream = new FileOutputStream(filesave);
											} catch (FileNotFoundException e1) {
												// TODO Auto-generated catch block
												e1.printStackTrace();
											}
											bitmapSingle.compress(Bitmap.CompressFormat.PNG, 100, outStream);
											try {
												outStream.flush();
												outStream.close();
											} catch (IOException e1) {
												// TODO Auto-generated catch block
												e1.printStackTrace();
											}


											/*Log.d("","2--Id" +inc);
											Toast.makeText(con, "INSIDE", 0).show();

											for(int i=0;i<totalSize;i++){

												images[i] = new ImageView(con);
												images[i].setId(i);
												LinearLayout.LayoutParams imageParams1 = new LinearLayout.LayoutParams(100, 100); 
												imageParams1.setMargins(5, 5, 5, 5);
												dynamicImagesAdd.addView(images[i],imageParams1);
											}

											for(int i=0;i<bitmap.size();i++){


												if(bitmap.get(i)!=null){

													images[i].setImageBitmap(bitmap.get(i));
												}

											}

											 */
											/*images[inc].setImageBitmap(bitmap);


											RelativeLayout.LayoutParams imageParams1 = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT); 

											if(totalSize == 1){

												imageParams1.addRule(RelativeLayout.BELOW, edit_addphoto.getId());
											}
											else{

												imageParams1.addRule(RelativeLayout.BELOW, images[inc].getId());
											}
											dynamicImagesAdd.addView(images[inc], imageParams1);*/

											// data has the bytes for the resume
										} else {
											// something went wrong
											e.printStackTrace();
										}
									}
								});

								//byte img[] = objects.get(i).getBytes("media_link");
								//edit_addphoto.setBackgroundResource(objects.get(i).getString("media_link"));

								//Toast.makeText(con, "OUTSIDE", 0).show();
								/*images[inc].setImageBitmap(bitmap);


								LinearLayout.LayoutParams imageParams1 = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT); 
								imageParams1.setMargins(5, 5, 5, 5);
								dynamicImagesAdd.addView(images[inc],imageParams1);*/
								/*	if(totalSize == 1){

									imageParams1.addRule(RelativeLayout.BELOW, edit_addphoto.getId());
								}
								if(totalSize>1){
									dynamicImagesAdd.addView(images[inc], imageParams1);
									imageParams1.addRule(RelativeLayout.BELOW, images[inc-1].getId());
								}
								dynamicImagesAdd.addView(images[inc], imageParams1);*/

								//Log.d("","ImageId" + images[inc].getId());

							}catch(NullPointerException ex){

								ex.printStackTrace();
							}
							catch (Exception e2) {
								// TODO: handle exception

								e2.printStackTrace();
							}

						}
					}
				});

			}else{

				Toast.makeText(con, "No internet connection", 0).show();
			}

			categoryname.setText(cat_title);
			editAdd.setText(tableAdd);
			edit_latitude.setText(latitude);
			edit_longitude.setText(longitude);
			edit_decription.setText(description);
			edit_date.setText(sdFormat1.format(dt2));
			edit_time.setText(sdFormat2.format(dt2));


			/*edit_latitude.setEnabled(false);
			edit_longitude.setEnabled(false);
			 */			
			if(cur_user.equals(tableUserId)){

				//Log.d("chk", "GETTINUSER =-=-=-=-=---=" + tableUserId);

				//Toast.makeText(con, "matched", 0).show();
				updatePin.setVisibility(View.VISIBLE);
				img_header.setText("Edit Incident");
				remove.setVisibility(View.VISIBLE);

				//list.setVisibility(View.VISIBLE);

				try {

					catTitle = db.catTitleFunc();
					catObjId = db.catObjIDFunc();

					//categoryname.setVisibility(View.GONE);

					categoryname.setOnClickListener(new OnClickListener() {

						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub

							/*							categoryname.setVisibility(View.GONE);
							list.setVisibility(View.VISIBLE);*/
							list.performClick();
						}
					});

					list.setAdapter(new ArrayAdapter<String>(con, android.R.layout.simple_spinner_dropdown_item, catTitle));
					list.setSelection(findPositionForSpinner(cat_title));
					list.setOnItemSelectedListener(new OnItemSelectedListener() {

						@Override
						public void onItemSelected(AdapterView<?> parent, View view,
								int pos, long id) {
							// TODO Auto-generated method stub


							/*	Toast.makeText(parent.getContext(), 
									"OnItemSelectedListener : " + parent.getItemAtPosition(pos).toString() + catObjId.get(pos),
									Toast.LENGTH_SHORT).show();

							if(parent.getItemAtPosition(pos).toString().equals(cat_title)){

								Toast.makeText(parent.getContext(), 
										"OnItemSelectedListener : " + catObjId.get(pos),
										Toast.LENGTH_SHORT).show();

							}
							 */
							category_put  = catObjId.get(pos);

							categoryname.setText(catTitle.get(pos));

						}

						@Override
						public void onNothingSelected(AdapterView<?> arg0) {
							// TODO Auto-generated method stub

						}
					});


				}catch(SQLException sql){

					sql.printStackTrace();
				}
				catch (Exception e) {
					// TODO: handle exception

					e.printStackTrace();
				}

				/*
				 * To change Image 
				 */


				edit_addphoto.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						AlertDialog.Builder  alertDialog = new AlertDialog.Builder(con);
						alertDialog.setIcon(R.drawable.ic_launcher);
						alertDialog.setTitle("SafeCity");
						alertDialog.setMessage("Choose Image From :");
						alertDialog.setCancelable(true);
						alertDialog.setPositiveButton("Gallery", new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog, int which) {
								// TODO Auto-generated method stub
								Intent intent = new Intent();
								intent.setType("image/*");
								intent.setAction(Intent.ACTION_GET_CONTENT);

								startActivityForResult(intent, 10);
								//overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
							}
						});
						alertDialog.setNegativeButton("Take Picture", new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog, int which) {
								// TODO Auto-generated method stub
								Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

								startActivityForResult(takePictureIntent, 2);	
								//overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
							}
						});
						AlertDialog alert = alertDialog.create();
						alert.show();
					}
				});


				/*
				 * To change date/time
				 */

				Calendar c = Calendar.getInstance();
				Date dt = c.getTime();
				year = c.get(Calendar.YEAR);
				month = c.get(Calendar.MONTH);
				day = c.get(Calendar.DAY_OF_MONTH);
				hour = c.get(Calendar.HOUR_OF_DAY);
				minute = c.get(Calendar.MINUTE);



				edit_date.setCursorVisible(false);
				edit_date.setInputType(InputType.TYPE_NULL);

				edit_time.setCursorVisible(false);
				edit_time.setInputType(InputType.TYPE_NULL);

				imm 			= (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
				this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

				edit_date.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						imm.hideSoftInputFromWindow(edit_date.getWindowToken(), 0);
						showDialog(DATE_DIALOG_ID);


					}
				});


				edit_time.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						imm.hideSoftInputFromWindow(edit_time.getWindowToken(), 0);
						showDialog(TIME_DIALOG_ID);

					}
				});


				// changed code for lat/long

				edit_latitude.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub

						Intent intent=new Intent(con,EditLatLng.class);
						startActivityForResult(intent, 1);
						overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);

					}
				});


				edit_longitude.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub

						Intent intent=new Intent(con,EditLatLng.class);
						startActivityForResult(intent, 1);
						overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
					}
				});

			}
			else{
				//Log.d("chk", "NOTGETTIN =-=-=-=-=---=" + tableUserId);
				//Toast.makeText(con, "NOTmatched", 0).show();

				editAdd.setFocusable(false);
				edit_latitude.setFocusable(false);
				edit_longitude.setFocusable(false);
				edit_decription.setFocusable(false);
				edit_date.setFocusable(false);
				edit_time.setFocusable(false);
				edit_addphoto.setFocusable(false);
				text_addphoto.setText("View photo");
			}



		}catch(SQLException sq){
			sq.printStackTrace();
		}
		catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

		/*
		 *  To remove the pin
		 */

		remove.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub




				if (netInfo != null && netInfo.isConnected()) {

					//Toast.makeText(con, "Connected", 0).show();

					FlurryAgent.logEvent("Delete_Pin_Event");

					ParseQuery deleteObjQuery = new ParseQuery("Incident_SafeCity");

					final ParseQuery deleteIncidentPerson = new ParseQuery("Incident_Person");

					final ParseQuery deleteImage = new ParseQuery("Media");

					final String takenObjId = objId;

					//deleteObj.whereEqualTo("objectId", objId);

					deleteObjQuery.getInBackground(objId, new GetCallback() {
						public void done(ParseObject object, ParseException e) {
							if (e == null) {
								// object will be your game score

								object.deleteInBackground(new DeleteCallback() {

									@Override
									public void done(ParseException e) {
										// TODO Auto-generated method stub

										if(e == null){

											deleteIncidentPerson.whereEqualTo("incident_id", takenObjId);

											deleteImage.whereEqualTo("incident_id", takenObjId);

											deleteIncidentPerson.findInBackground(new FindCallback() {

												@Override
												public void done(List<ParseObject> objects, ParseException e) {
													// TODO Auto-generated method stub

													for(int i=0;i<objects.size();i++){

														objects.get(i).deleteInBackground();
													}
												}
											});


											deleteImage.findInBackground(new FindCallback() {

												@Override
												public void done(List<ParseObject> objects, ParseException e) {
													// TODO Auto-generated method stub

													for(int i=0;i<objects.size();i++){

														objects.get(i).deleteInBackground();
													}
												}
											});
										}
										else{

											e.printStackTrace();
										}
									}
								});



							} else {
								// something went wrong

								e.printStackTrace();
							}
						}
					});

					try {

						db.open();
						db.delete_byID(objId);
						db.close();

					}catch(SQLException sql){

						sql.printStackTrace();
					}
					catch (Exception e2) {
						// TODO: handle exception
						e2.printStackTrace();
					}


					Intent intent=new Intent(getApplicationContext(),PinList.class);
					startActivity(intent);
					finish();
					overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);

				}
				else{

					Toast.makeText(con, "No internet connection", 0).show();
				}


			}

		});


		/*
		 * 
		 * To share Pin
		 */

		sharePin.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				/*
				File cacheDir = new File(android.os.Environment.getExternalStorageDirectory(), getString(getApplicationInfo().labelRes));
				File f = new File(cacheDir, "sal.jpg");*/



				Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
				sharingIntent.setType("text/plain");
				sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "SafeCity");
				sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, "Address: " + tableAdd + "\n" + "Reason: " + cat_title + "\n" + "Description: " + description + "\n" + "Date: " +sdFormat1.format(dt2) + " " + sdFormat2.format(dt2));

				if(filesave.exists() && edit_addphoto.getDrawable()!=null){

					sharingIntent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(filesave));
				}


				/*sharingIntent.setType("image/*");
				sharingIntent.putExtra(Intent.EXTRA_STREAM, Uri.parse("/sdcard/sal.JPG"));*/

				//	startActivity(Intent.createChooser(sharingIntent, "Share via"));

				startActivity(Intent.createChooser(sharingIntent, "Send mail..."));
				overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);

			}
		});

		/*
		 * To update data of Pin
		 */

		updatePin.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub


				if (netInfo != null && netInfo.isConnected()) {

					FlurryAgent.logEvent("Update_Pin_Event");

					ParseQuery updateQuery = new ParseQuery("Incident_SafeCity");

					final ParseQuery udateImage = new ParseQuery("Media");


					String add =  editAdd.getText().toString();
					String desc =  edit_decription.getText().toString();

					double lat = Double.parseDouble(edit_latitude.getText().toString());
					double lon = Double.parseDouble(edit_longitude.getText().toString());

					final ParseGeoPoint changedPoint = new ParseGeoPoint(lat, lon);
					//String updatedCat = categoryname.getText().toString();


					//------------ code to change date ---------------

					/*SimpleDateFormat dateFormat = new SimpleDateFormat("MM-dd-yyyy HH:mm"); 
					try {

						date1 = dateFormat.parse(edit_date.getText().toString()+" "+edit_time.getText().toString());
						Toast.makeText(con, date1.toString(), Toast.LENGTH_SHORT).show();

					} catch (java.text.ParseException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					 */
					//tmpDate = 	date1.getTime();

					//Toast.makeText(con, "" + tmpDate, Toast.LENGTH_SHORT).show();

					updateQuery.getInBackground(objId, new GetCallback() {

						@Override
						public void done(ParseObject object, ParseException e) {
							// TODO Auto-generated method stub


							if(e == null){

								object.put("location", changedPoint );
								object.put("address", editAdd.getText().toString());	
								object.put("description", edit_decription.getText().toString() + "");
								//object.put("active", 1);
								//object.put("mode", 5);
								//object.put("verified", 1);
								object.put("category_id", category_put);
								//object.put("date", date1);
								//object.put("user_id", cur_user);
								object.saveInBackground(new SaveCallback() {

									@Override
									public void done(ParseException e) {
										// TODO Auto-generated method stub

										if(e == null){

											udateImage.whereEqualTo("incident_id", objId);
											udateImage.findInBackground(new FindCallback() {

												@Override
												public void done(List<ParseObject> objects, ParseException e) {
													// TODO Auto-generated method stub

													if(objects.size()>0){

														for(int i=0;i<objects.size();i++){

															if(parseFile == null){

															}
															else{


																objects.get(i).put("media_link", parseFile);
																objects.get(i).saveInBackground();
															}

														}
													}
													else{


														if(parseFile == null){

														}
														else{


															ParseObject objctArr = new ParseObject("Media");

															objctArr.put("active", 1);
															objctArr.put("media_type", 1);
															//objctArr.put("media_date", date1);
															objctArr.put("incident_id", objId);
															objctArr.put("media_link", parseFile);
															objctArr.saveInBackground();

														}
													}


												}
											});

										}
										else{

											e.printStackTrace();
										}

									}
								});

							}
							else{

								e.printStackTrace();
							}


						}
					});


					try {

						db.open();
						db.updateRecord(objId, desc,add,category_put);
						db.close();

					}catch(SQLException sql){

						sql.printStackTrace();
					}
					catch (Exception e2) {
						// TODO: handle exception
						e2.printStackTrace();
					}


					Intent intent=new Intent(getApplicationContext(),PinList.class);
					startActivity(intent);
					finish();
					overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);

				}
				else{

					Toast.makeText(con, "No internet connection", 0).show();
				}



			}


		});

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_edit_show_pin, menu);
		return false;
	}


	private void showImage(String receivedImage)   {
		if(!(path.equalsIgnoreCase("") )){
			Log.i("showImage","loading:"+path);
			//imagetext.setVisibility(View.GONE);
			BitmapFactory.Options bfOptions=new BitmapFactory.Options();
			bfOptions.inDither=false;                     //Disable Dithering mode
			bfOptions.inPurgeable=true;                   //Tell to gc that whether it needs free memory, the Bitmap can be cleared
			bfOptions.inInputShareable=true;			//Which kind of reference will be used to recover the Bitmap data after being clear, when it will be used in the future
			bfOptions.inSampleSize=4;

			bfOptions.inTempStorage=new byte[32 * 1024]; 


			File file=new File(path);

			//Toast.makeText(getApplicationContext(), " " + path, Toast.LENGTH_LONG).show();



			try{

				/*FileInputStream fis = new FileInputStream(file);
				Bitmap bi = BitmapFactory.decodeStream(fis);
				ByteArrayOutputStream baos = new ByteArrayOutputStream();
				bi.compress(Bitmap.CompressFormat.PNG, 100, baos);
				data = baos.toByteArray();*/


				BitmapFactory.Options options = new BitmapFactory.Options();
				options.inSampleSize = 2;
				Bitmap bm = BitmapFactory.decodeFile(path, options);

				options.inJustDecodeBounds = true;
				options.inPurgeable=true;
				options.inDither=false;
				options.inPreferredConfig = Bitmap.Config.RGB_565;

				ByteArrayOutputStream stream = new ByteArrayOutputStream();

				bm.compress(Bitmap.CompressFormat.PNG, 0, stream);

				byteArray = stream.toByteArray();


			}catch(Exception e){
				e.printStackTrace();

			}

			Date timestamp = new Date();
			Timestamp imgname = new Timestamp(timestamp.getTime());
			String test  = imgname.toString();
			Log.d("STRING","STRING" +  test);

			parseFile = new ParseFile("img.png",byteArray);
			parseFile.saveInBackground();

			FileInputStream fs=null;
			try {
				fs = new FileInputStream(file);
			} catch (FileNotFoundException e) {
				//TODO do something intelligent
				e.printStackTrace();
			}

			Bitmap bm = null;
			try {
				if(fs!=null) bm=BitmapFactory.decodeFileDescriptor(fs.getFD(), null, bfOptions);
			} catch (IOException e) {
				//TODO do something intelligent
				e.printStackTrace();
			} finally{ 
				if(fs!=null) {
					try {
						fs.close();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
			//bm=BitmapFactory.decodeFile(path, bfOptions); This one causes error: java.lang.OutOfMemoryError: bitmap size exceeds VM budget

			edit_addphoto.setImageBitmap(bm);
			//tabImage.setImageBitmap(bm);
			//bm.recycle();
			bm=null;
		}else{
			/*captureImage.setImageResource(R.drawable.capture_frame);
			tabImage.setImageResource(R.drawable.frame);
			imagetext.setVisibility(View.VISIBLE);*/
		}
	}


	private String getPath(Uri selectedImage) {
		// TODO Auto-generated method stub

		String[] projection = { MediaStore.Images.Media.DATA };
		Cursor cursor = getContentResolver().query(selectedImage, projection, null, null, null);
		int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
		cursor.moveToFirst();
		return cursor.getString(column_index);

	}


	public String getRealPathFromURI(Uri contentUri) {
		// can post image
		String [] proj={MediaStore.Images.Media.DATA};
		Cursor cursor = getContentResolver().query(contentUri,
				proj, // Which columns to return
				null, // WHERE clause; which rows to return (all rows)
				null, // WHERE clause selection arguments (none)
				null); // Order-by clause (ascending by name)
		int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
		cursor.moveToFirst();
		return cursor.getString(column_index);
	}


	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub

		super.onActivityResult(requestCode, resultCode, data);

		if(data == null || resultCode == 0){
			path="";
		}else if(requestCode == 2){

			/*layout.setBackgroundResource(R.drawable.published_page);*/
			thumbnail = (Bitmap) data.getExtras().get("data");
			final Uri contentUri = data.getData();
			path = getPath(contentUri);
			showImage(path);

		}

		else if(requestCode == 10){
			Uri selectedImage = data.getData();
			try {

				path = getRealPathFromURI(selectedImage);
				showImage(path);


			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		if(requestCode == 1 ){

			if( resultCode == RESULT_OK){

				//double chk = Double.parseDouble(data.getStringExtra("lati"));

				edit_latitude.setText(data.getStringExtra("latitude"));
				edit_longitude.setText(data.getStringExtra("longitude"));
				editAdd.setText(data.getStringExtra("address"));


				/*Toast.makeText(con, "" + chk, 0).show();
				Log.d("RESULT", "RESULT" + chk);*/

			}
			/*	edit_latitude.setText(data.getDoubleExtra("latitude", 0)+"");
			edit_longitude.setText(data.getDoubleExtra("longitude", 0)+"");
			editAdd.setText(data.getStringExtra("address"));


			Toast.makeText(con, "" + editAdd.getText().toString(), 0).show();
			Log.d("RESULT", "RESULT" + edit_latitude.toString());*/
		}


	}


	@Override
	protected Dialog onCreateDialog(int id) {
		switch (id) {
		case DATE_DIALOG_ID:
			// set date picker as current date
			return new DatePickerDialog(this, datePickerListener, year, month,day);

		case TIME_DIALOG_ID:
			// set time picker as current time
			return new TimePickerDialog(this, timePickerListener, hour, minute,false);

		}

		return null;
	}

	private DatePickerDialog.OnDateSetListener datePickerListener = new DatePickerDialog.OnDateSetListener() {

		// when dialog box is closed, below method will be called.
		public void onDateSet(DatePicker view, int selectedYear,
				int selectedMonth, int selectedDay) {
			year = selectedYear;
			month = selectedMonth;
			day = selectedDay;

			// set selected date into textview
			edit_date.setText(new StringBuilder().append(month + 1).append("-").append(day).append("-").append(year));

			// set selected date into datepicker also
			/*			dpResult.init(year, month, day, null);
			 */
		}
	};


	private TimePickerDialog.OnTimeSetListener timePickerListener =   new TimePickerDialog.OnTimeSetListener() {
		public void onTimeSet(TimePicker view, int selectedHour,int selectedMinute) {
			hour = selectedHour;
			minute = selectedMinute;

			// set current time into textview
			edit_time.setText(new StringBuilder().append(pad(hour)).append(":").append(pad(minute)));

			// set current time into timepicker
			/*timePicker1.setCurrentHour(hour);
			timePicker1.setCurrentMinute(minute);*/

		}
	};


	private static String pad(int c) {
		if (c >= 10)
			return String.valueOf(c);
		else
			return "0" + String.valueOf(c);
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub

		Intent intent=new Intent(getApplicationContext(),PinList.class);
		startActivity(intent);
		finish();
		overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);

		super.onBackPressed();
	}

	/*public static void SelectSpinnerItemByValue(Spinner spnr, long value)
	{
	    SimpleCursorAdapter adapter = (SimpleCursorAdapter) spnr.getAdapter();
	    for (int position = 0; position < adapter.getCount(); position++)
	    {
	        if(adapter.getItemId(position) == value)
	        {
	            spnr.setSelection(position);
	            return;
	        }
	    }
	}*/

	public int findPositionForSpinner(String category_to_select){

		int pos = 0;
		for(int i=0;i<catTitle.size();i++){
			if(category_to_select.equalsIgnoreCase(catTitle.get(i))){
				pos = i;
			}

		}
		return pos;


	}

	@Override
	protected void onStart() {
		// TODO Auto-generated method stub
		super.onStart();

		FlurryAgent.onStartSession(this, "XYCF767S2WQJ6TR2VR2B");
	}

	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		super.onStop();

		FlurryAgent.onEndSession(this);
	}



}
